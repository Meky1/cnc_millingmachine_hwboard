param ($BuildFolder)
$TestFolder = 'Tests'

Set-Location $TestFolder 
cmake --build $BuildFolder -j6
Set-Location $BuildFolder
ctest -VV
Set-Location ..