
param ($BuildFolder)
$TestFolder = 'Tests'

Set-Location $TestFolder

if (Test-Path -Path $BuildFolder) {
    Remove-Item -r $BuildFolder
}

mkdir $BuildFolder 
Set-Location $BuildFolder 
cmake -G "MinGW Makefiles" ..