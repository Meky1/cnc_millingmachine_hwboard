
param ($BuildFolder)

if (Test-Path -Path $BuildFolder) {
    Remove-Item -r $BuildFolder
}

mkdir $BuildFolder 
Set-Location $BuildFolder 
cmake -G "MinGW Makefiles" ..