/*****************************************************************************
 * @file app/SystemManager/systemManager.c
 *
 * @brief User application main
 *
 * @author
 * @date 03.08.2022
 * @version v1.0
 *
 ****************************************************************************/

#include "driver/AssertDebug/debug.h"

#include "middleware/CommandReception/commandReception.h"
#include "middleware/GcodeExecute/gcodeExecute.h"
#include "middleware/StateMachine/stateMachine.h"

#include "app/SystemManager/systemManager.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#define LOG_MODULE_NAME "[SYS_MANAGER]"

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
*****************************************************************************/
//clang-format off

typedef enum {
    SYS_MANAGER_RECEIVE_GCODE_LINE = 0,
    SYS_MANAGER_PARSE_GCODE_LINE,
    SYS_MANAGER_EXECUTE_GCODE_LINE,
    SYS_MANAGER_FAIL,
} systemManagerStateMachine_t;

//clang-format on

static stateMachine_t sSystemManagerStateMachine = { 0 };

/*****************************************************************************
                            STATE MACHINE FUNCTIONS
 *****************************************************************************/

/*************************************************************************************************
 *  @brief      Receive GCode line from the CommandReception module.                            **
 *                                                                                              **
 *  @return     State machine status (check for stateResult_t definition).                      **
 *                                                                                              **
 *************************************************************************************************/
static stateResult_t SystemManagerReceiveGcodeLine(void);

/*************************************************************************************************
 *  @brief      Receive line process.                                                           **
 *                                                                                              **
 *  @return     State machine status (check for stateResult_t definition).                      **
 *                                                                                              **
 *************************************************************************************************/
static stateResult_t SystemManagerParseGcodeLine(void);

/*************************************************************************************************
 *  @brief      Receive complete and cleaning process.                                          **
 *                                                                                              **
 *  @return     State machine status (check for stateResult_t definition).                      **
 *                                                                                              **
 *************************************************************************************************/
static stateResult_t SystemManagerExecuteGcodeLine(void);

/*************************************************************************************************
 *  @brief      Function process for an occurrence of a fail.                                   **
 *                                                                                              **
 *  @return     State machine status (check for stateResult_t definition).                      **
 *                                                                                              **
 *************************************************************************************************/
static stateResult_t SystemManagerFail(void);

static state_t sSystemManagerStates[] = { [SYS_MANAGER_RECEIVE_GCODE_LINE] = { "SYS_MANAGER_RECEIVE_GCODE_LINE", 0, SystemManagerReceiveGcodeLine, SYS_MANAGER_FAIL, 0 },
                                          [SYS_MANAGER_PARSE_GCODE_LINE] = { "SYS_MANAGER_PARSE_GCODE_LINE", 0, SystemManagerParseGcodeLine, SYS_MANAGER_FAIL, 0 },
                                          [SYS_MANAGER_EXECUTE_GCODE_LINE] = { "SYS_MANAGER_EXECUTE_GCODE_LINE", 0, SystemManagerExecuteGcodeLine, SYS_MANAGER_FAIL, 0 },
                                          [SYS_MANAGER_FAIL] = { "SYS_MANAGER_FAIL", 0, SystemManagerFail, SYS_MANAGER_FAIL, 0 } };

/*****************************************************************************
                         PRIVATE FUNCTION DECLARATION
*****************************************************************************/

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

bool SystemManagerInit(void)
{
    bool status = true;

    status &= StateMachineInit(&sSystemManagerStateMachine, "SYS_MANAGER_MACHINE", sSystemManagerStates, ARRAY_SIZE(sSystemManagerStates));

    return status;
}

void SystemManagerProcess(void)
{
    StateMachineProcess(&sSystemManagerStateMachine);
}

/******************************************************************************
                      STATE MACHINE FUNCTION IMPLEMENTATION
 ******************************************************************************/

static stateResult_t SystemManagerReceiveGcodeLine(void)
{

    return STATE_MACHINE_OK;
}

static stateResult_t SystemManagerParseGcodeLine(void)
{

    return STATE_MACHINE_OK;
}

static stateResult_t SystemManagerExecuteGcodeLine(void)
{

    return STATE_MACHINE_OK;
}

static stateResult_t SystemManagerFail(void)
{

    return STATE_MACHINE_OK;
}

/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
******************************************************************************/
