/*****************************************************************************
 * @file app/SystemManager/systemManager.h
 *
 * @brief User application main
 *
 * @author
 * @date 03.08.2022
 * @version v1.0
 *
 ****************************************************************************/

#pragma once

#include "CncConfig.h"

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
*****************************************************************************/

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
*****************************************************************************/

bool SystemManagerInit(void);

/**
 * Main user application - loop process of the system.
 */
void SystemManagerProcess(void);
