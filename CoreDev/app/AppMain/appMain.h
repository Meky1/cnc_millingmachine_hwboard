/*****************************************************************************
 * @file app/appMain/appMain.h
 *
 * @brief User application main
 *
 * @author
 * @date 03.08.2022
 * @version v1.0
 *
 ****************************************************************************/

#pragma once

#include "CncConfig.h"

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
*****************************************************************************/

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
*****************************************************************************/

/**
 * Main user application - initialization of the system.
 */
void AppMainInit(void);

/**
 * Main user application - loop process of the system.
 */
void AppMainProcess(void);
