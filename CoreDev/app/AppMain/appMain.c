/*****************************************************************************
 * @file app/appMain/appMain.c
 *
 * @brief User application main
 *
 * @author
 * @date 03.08.2022
 * @version v1.0
 *
 ****************************************************************************/

#include "app/SystemManager/systemManager.h"
#include "driver/AssertDebug/debug.h"
#include "middleware/CommandReception/commandReception.h"
#include "middleware/StepperMotor/stepperMotor.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/
#ifdef CFG_DEBUG_MODULE_NAME
#undef CFG_DEBUG_MODULE_NAME
#endif

#define CFG_DEBUG_MODULE_NAME "[CNC APP] "

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
*****************************************************************************/


/*****************************************************************************
                         PRIVATE FUNCTION DECLARATION
*****************************************************************************/


/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

void AppMainInit(void)
{
    LOG_INFO("Hello and welcome to the CNC app!");

    bool status = true;

    status &= CommandReceptionInit();
    status &= StepperMotorInit();
}

void AppMainProcess(void)
{
    SystemManagerProcess();
}

/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
******************************************************************************/