/*****************************************************************************
 *
 * @file cncConfig.h
 *
 * @brief Configuration file for the CNC Milling Machine system
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/

#pragma once

#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
*****************************************************************************/

/* Configuration flags.
****************************************************************/
#define CFG_DEBUG (1U)
#define CFG_RELEASE (2U)
#define CFG_BUILD_TYPE CFG_DEBUG

/* Utils.
****************************************************************/
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
#define GCODE_LINE_LENGTH (128U)
#define PACKED __attribute__((packed, aligned(1)))

/* System error codes.
****************************************************************/
typedef enum {

    FAIL = 0x00,

    COMMAND_HEADER_OK = 0x10,
    COMMAND_DATA_OK = 0x11,
    COMMAND_READY = 0x12,
    COMMAND_CRC_FAILURE = 0x13,
    COMMAND_TRANSMISSION_TIMEOUT = 0x14,

    GCODE_EXECUTION_SUCCESSFUL = 0x20,
    GCODE_EXECUTION_IN_PROGRESS = 0x21,
    GCODE_EXECUTION_FAILED = 0x22,

    GCODE_PARSE_OK = 0x31,
    GCODE_PARSE_FAILURE = 0x32,

} systemErrorCodes_t;

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
*****************************************************************************/


/****************************** END OF FILE  *********************************/
