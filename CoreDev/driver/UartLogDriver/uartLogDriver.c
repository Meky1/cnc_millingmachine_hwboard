/*****************************************************************************
 * @file uartLogDriver.c
 *
 * @brief Source file for the UART LOG driver
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/

#include "driver/UartLogDriver/uartLogDriver.h"
#include "driver/AssertDebug/debug.h"

#include "main.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#define TX_LOG_MAX_LENGTH (1024U)
#define UART_LOG_TIMEOUT (50U)
#define ENDLINE_SYMBOL_LENGTH (3U)

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
*****************************************************************************/

static char sLogBufferTx[TX_LOG_MAX_LENGTH] = { 0 };

extern UART_HandleTypeDef huart1;

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

void UartLogDriverPrint(const char *format, va_list args)
{
    ASSERT(format);

    vsnprintf(sLogBufferTx, TX_LOG_MAX_LENGTH, format, args);
    strncat(sLogBufferTx, "\r\n", ENDLINE_SYMBOL_LENGTH);

    HAL_UART_Transmit(&huart1, (uint8_t *)sLogBufferTx, TX_LOG_MAX_LENGTH, UART_LOG_TIMEOUT);

    memset(sLogBufferTx, 0, strlen(sLogBufferTx));
}


/****************************** END OF FILE  *********************************/
