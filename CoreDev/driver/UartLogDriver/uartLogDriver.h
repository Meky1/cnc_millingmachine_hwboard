/*****************************************************************************
 * @file uartLogDriver.h
 *
 * @brief Header file for the UART LOG driver
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/

#pragma once

#include "CncConfig.h"

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
*****************************************************************************/

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

/*************************************************************************************************
 * @brief           Send the given string to the terminal using variadic list.                  **
 *                                                                                              **
 * @param format    String to be sent to the terminal.                                          **
 * @param args      Variadic list containing arguments.                                         **
 *                                                                                              **
 *************************************************************************************************/
void UartLogDriverPrint(const char *format, va_list args);


/****************************** END OF FILE  *********************************/
