/*****************************************************************************
 * @file timeBaseModule.h
 *
 * @brief
 *
 * @author Marcin Czarnik
 * @date 08.04.2021
 * @version v1.0
 *
 ****************************************************************************/
#pragma once

#include <stdbool.h>
#include <stdint.h>

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

/*************************************************************************************************
 *  @brief              Get tick for the time base.                                             **
 *                                                                                              **
 *************************************************************************************************/
uint32_t TimeBaseDriverGetTick(void);

/*************************************************************************************************
 *  @brief              Check if time elapsed.                                                  **
 *                                                                                              **
 *  @param startTime                                                                            **
 *  @param deltaMsTime  Delta time in miliseconds.                                              **
 *                                                                                              **
 *  @return             True if set time has passed, otherwise false.                           **
 *                                                                                              **
 *************************************************************************************************/
bool TimeBaseDriverHasTimeElapsed(uint32_t startTime, uint32_t deltaMsTime);

/*************************************************************************************************
 *  @brief              Simple delay based on HAL.                                              **
 *                                                                                              **
 *  @param delayTime    Time to delay in miliseconds                                            **
 *                                                                                              **
 *************************************************************************************************/
void DelayMs(uint32_t delayTime);
