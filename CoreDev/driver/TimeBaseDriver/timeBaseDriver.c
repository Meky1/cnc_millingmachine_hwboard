/*****************************************************************************
 * @file timeBaseModule.c
 *
 * @brief
 *
 * @author Marcin Czarnik
 * @date 08.04.2021
 * @version v1.0
 *
 ****************************************************************************/

#include "driver/TimeBaseDriver/timeBaseDriver.h"
#include "main.h"

extern TIM_HandleTypeDef htim7;

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

uint32_t TimeBaseDriverGetTick(void)
{
    return (htim7.Instance->CNT);
}

bool TimeBaseDriverHasTimeElapsed(uint32_t startTime, uint32_t deltaMsTime)
{
    if (TimeBaseDriverGetTick() - startTime > deltaMsTime) {
        return true;
    }
    return false;
}

void DelayMs(uint32_t delayTime)
{
    HAL_Delay(delayTime);
}
