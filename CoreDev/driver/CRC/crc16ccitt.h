/*****************************************************************************
 *
 * @file middleware/CRC/crc16ccitt.c
 *
 * @brief Header file for the CRC-16 CCITT implementation.
 *
 * @author Marcin Czarnik
 * @date 03.08.2022
 * @version v1.0
 *
 ****************************************************************************/

#pragma once

#include "CncConfig.h"

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
*****************************************************************************/

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
*****************************************************************************/

/*************************************************************************************************
 *  @brief           Calculates the CRC-16 CCITT.                                               **
 *                                                                                              **
 *  @param data      The pointer to the array of the data which we want to calculate.           **
 *  @param length    Size of the data.                                                          **
 *                                                                                              **
 *  @return          The status of the calculated CRC.                                          **
 *                                                                                              **
 *************************************************************************************************/
uint16_t CrcCalculate16bCRC(uint8_t *data, uint16_t length);
