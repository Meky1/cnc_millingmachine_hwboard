/*****************************************************************************
 *
 * @file middleware/CRC/crc16ccitt.c
 *
 * @brief Source file for the CRC-16 CCITT implementation.
 *
 * @author Marcin Czarnik
 * @date 03.08.2022
 * @version v1.0
 *
 ****************************************************************************/

#include "driver/CRC/crc16ccitt.h"
#include "driver/AssertDebug/debug.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#define LOG_MODULE_NAME "[GCODE_PARSE]"
#define BYTE_SIZE (8u)

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
*****************************************************************************/

/*****************************************************************************
                         PRIVATE FUNCTION DECLARATION
*****************************************************************************/

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

uint16_t CrcCalculate16bCRC(uint8_t *data, uint16_t length)
{
    ASSERT(data);
    ASSERT(length > 0u);

    uint16_t crc = 0u;

    for (uint16_t i = 0; i < length; i++) {
        crc = crc ^ ((uint16_t)data[i] << BYTE_SIZE);
        for (uint8_t i = 0u; i < BYTE_SIZE; i++) {
            if (crc & 0x8000u) {
                crc = (crc << 1u) ^ 0x1021u;
            }
            else {
                crc = crc << 1u;
            }
        }
    }

    return crc;
}

/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
******************************************************************************/