/*****************************************************************************
 *
 * @file debug.c
 *
 * @brief Source file of the debug implementation - redirection to printf
 *
 * @author Marcin Czarnik
 * @version v1.0
 *
 ****************************************************************************/

#include "driver/AssertDebug/debug.h"
#include "driver/McuDriver/mcuDriver.h"
#include "driver/UartLogDriver/uartLogDriver.h"

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
 *****************************************************************************/

void _dbg_usart_printf(const char *format, ...)
{
    if (McuDriverIsInInterrupt()) {
        return;
    }

    va_list args;
    va_start(args, format);
    UartLogDriverPrint(format, args);
    va_end(args);
}

void _dbg_reset()
{
    McuDriverSystemReset();
}

void _dbg_break()
{
    McuDriverInsertBreakpoint();
}

void _dbg_panic_loop(const char *format, ...)
{
    uint16_t printDelayCount = 0;

    __disable_irq();

    while (1) {
        HAL_Delay(500);

        printDelayCount++;

        HAL_Delay(500);

        // 20 = 10(sec)  because of DelayUs for 500(ms)
        if ((!McuDriverIsInInterrupt()) && (printDelayCount > 10)) {
            va_list args;
            va_start(args, format);
            UartLogDriverPrint(format, args);
            va_end(args);

            printDelayCount = 0;
        }
    }
}

/****************************** END OF FILE  *********************************/
