/*****************************************************************************
 * @file debug.h
 *
 * @brief Header file of the debug implementation - redirection to printf
 *
 * @author Marcin Czarnik
 * @version v1.0
 *
 ****************************************************************************/

#pragma once

#include "CncConfig.h"

/*****************************************************************************
                                    CONFIG
*****************************************************************************/

/* Configuration for tests.
****************************************************************/

#define CFG_DEBUG_USART_ENABLE (1)
#define CFG_DEBUG_LOG_ENABLE (1)
#define CFG_DEBUG_ASSERT_ENABLE (1)

/* Special attributes for clang-tidy.
****************************************************************/
#ifdef __clang_analyzer__
#define NORETURN __attribute__((analyzer_noreturn))
#else
#define NORETURN
#endif

/* Check if build configuration type is defined.
****************************************************************/
#if !defined(CFG_BUILD_TYPE) || (CFG_BUILD_TYPE == 0)
#error Include CncConfig.h before including debug.h
#endif

/* Define if USART printf function shall be used.
****************************************************************/

#ifdef CFG_DEBUG_USART_ENABLE
/** @brief Print to USART */
void _dbg_usart_printf(const char *format, ...);

#define __DEBUG_INTERNAL_USART_PRINTF _dbg_usart_printf

#else
#define __DEBUG_INTERNAL_USART_PRINTF(...)
#endif

#define __DBG_PRINTF(...)                                                                                                                                                                                                                                                                                  \
    do {                                                                                                                                                                                                                                                                                                   \
        __DEBUG_INTERNAL_USART_PRINTF(__VA_ARGS__);                                                                                                                                                                                                                                                        \
    } while (0)

/*****************************************************************************
                                    LOGGING
*****************************************************************************/

/* Define if the module tag shall be used in the log.
****************************************************************/

#ifndef CFG_DEBUG_MODULE_NAME
#define CFG_DEBUG_MODULE_NAME ""
#endif

#ifdef CFG_DEBUG_LOG_ENABLE

#define LOG_INFO(...) __DBG_PRINTF("[INFO] " CFG_DEBUG_MODULE_NAME ": " __VA_ARGS__)
#define LOG_WARN(...) __DBG_PRINTF("[WARN] " CFG_DEBUG_MODULE_NAME ": " __VA_ARGS__)
#define LOG_ERROR(...) __DBG_PRINTF("[ERROR] " CFG_DEBUG_MODULE_NAME ": " __VA_ARGS__)

#else
#define LOG_INFO(...)
#define LOG_WARN(...)
#define LOG_ERROR(...)
#endif

/*****************************************************************************
                                DEBUG BLOCK
*****************************************************************************/
#if CFG_BUILD_TYPE == CFG_BUILD_RELEASE
#define DEBUG_BLOCK(...)
#else
#define DEBUG_BLOCK(...)                                                                                                                                                                                                                                                                                   \
    do {                                                                                                                                                                                                                                                                                                   \
        __VA_ARGS__;                                                                                                                                                                                                                                                                                       \
    } while (0)
#endif

/*****************************************************************************
                                STATIC ASSERT
*****************************************************************************/
#ifdef __cplusplus
#ifndef _Static_assert
#define _Static_assert static_assert
#endif
#endif

/** @brief Static assert */
#define STATIC_ASSERT(expression) _Static_assert((expression), #expression)

/*****************************************************************************
                                   ASSERT
*****************************************************************************/
#ifdef CFG_DEBUG_ASSERT_ENABLE

/** @brief Loop that device enters after failed assertion */
void _dbg_panic_loop(const char *format, ...) NORETURN;

/** @brief Set debug breakpoint */
void _dbg_break();

/** @brief Reset device - used in ASSERT only */
void _dbg_reset();

#if CFG_BUILD_TYPE == CFG_BUILD_RELEASE
#define __DEBUG_INTERNAL_BREAK_OR_RESET _dbg_reset
#else
#define __DEBUG_INTERNAL_BREAK_OR_RESET _dbg_break
#endif

#define ASSERT_MSG(_X, _Y)                                                                                                                                                                                                                                                                                 \
    do {                                                                                                                                                                                                                                                                                                   \
        if (!(_X)) {                                                                                                                                                                                                                                                                                       \
            LOG_ERROR("ASSERT FAILED\r\n");                                                                                                                                                                                                                                                                \
            LOG_ERROR("     Called from: %s:%d\r\n", __FILE__, __LINE__);                                                                                                                                                                                                                                  \
            LOG_ERROR("     Function:    %s\r\n", __FUNCTION__);                                                                                                                                                                                                                                           \
            LOG_ERROR("Message:     %s\r\n", _Y);                                                                                                                                                                                                                                                          \
            __DEBUG_INTERNAL_BREAK_OR_RESET();                                                                                                                                                                                                                                                             \
            _dbg_panic_loop("ASSERT FAILED\r\nCalled from: %s:%d\r\nFunction:    %s\r\nMessage:     %s\r\n", __FILE__, __LINE__, __FUNCTION__, (_Y));                                                                                                                                                      \
        }                                                                                                                                                                                                                                                                                                  \
    } while (0)


#define ASSERT(_X) ASSERT_MSG(_X, #_X)

#else
#define ASSERT(...)
#define ASSERT_MSG(_X, ...)
#endif

/****************************** END OF FILE  *********************************/
