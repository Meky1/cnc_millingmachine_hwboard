/*****************************************************************************
 *
 * @file  driver/SoftwareTimer/softwareTimer.c
 *
 * @brief Source file with time management implementation - includes time measurements and delay handling.
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/

#include "driver/SoftwareTimer/softwareTimer.h"
#include "driver/AssertDebug/debug.h"
#include "main.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#define TIME_MAIN_LOOP_FAST_INTERVAL (10u)  // [ms]

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
 *****************************************************************************/

extern TIM_HandleTypeDef htim6;

volatile uint32_t TimerMsGlobal;  // Global timer triggered every 1ms

/*****************************************************************************
                         PRIVATE FUNCTION DECLARATION
 *****************************************************************************/

/*****************************************************************************
                            INTERFACE IMPLEMENTATION
*****************************************************************************/

void SoftwareTimerInit(void)
{
    HAL_TIM_Base_Start_IT(&htim6);
}

uint32_t SoftwareTimerGetGlobalTime(void)
{
    __disable_irq();
    uint32_t time = (TimerMsGlobal * 1000U) + htim6.Instance->CNT;
    __enable_irq();

    return time;
}

void SoftwareTimerTrigger(void)
{
    static volatile uint16_t cntMainLoopFastTrigger = 0;

    TimerMsGlobal++;
    cntMainLoopFastTrigger++;

    if (cntMainLoopFastTrigger >= TIME_MAIN_LOOP_FAST_INTERVAL) {
        cntMainLoopFastTrigger = 0;
    }
}

void SoftwareTimerEventActivateMs(timeEvent_t *timeEvent, uint32_t delayMs)
{
    ASSERT(timeEvent);
    ASSERT(delayMs > 0);

    timeEvent->occurenceTimestamp = TimerMsGlobal + delayMs;
    timeEvent->enableFlag = true;
}

uint32_t SoftwareTimerEventPassedTime(timeEvent_t *timeEvent, uint32_t delayMs)
{
    ASSERT(timeEvent);
    ASSERT(delayMs > 0);

    if (timeEvent->enableFlag) {
        return delayMs - (timeEvent->occurenceTimestamp - TimerMsGlobal);
    }

    return 0;
}

void SoftwareTimerEventMoveMs(timeEvent_t *timeEvent, uint32_t delayMs)
{
    ASSERT(timeEvent);
    ASSERT(delayMs > 0);

    timeEvent->occurenceTimestamp += delayMs;
    timeEvent->enableFlag = true;
}

void SoftwareTimerEventDeactivate(timeEvent_t *timeEvent)
{
    ASSERT(timeEvent);

    timeEvent->enableFlag = false;
}

bool SoftwareTimerIsEventPending(timeEvent_t *timeEvent)
{
    ASSERT(timeEvent);

    return timeEvent->enableFlag;
}

bool SoftwareTimerIsEventReady(timeEvent_t *timeEvent)
{
    ASSERT(timeEvent);

    bool isEventReady = false;

    if (timeEvent->enableFlag) {
        if ((timeEvent->occurenceTimestamp - TimerMsGlobal) & 0x80000000) {
            timeEvent->enableFlag = false;
            isEventReady = true;
        }
    }

    return isEventReady;
}

/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
 ******************************************************************************/



/****************************** END OF FILE  *********************************/
