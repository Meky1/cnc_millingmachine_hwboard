/*****************************************************************************
 *
 * @file  driver/SoftwareTimer/softwareTimer.h
 *
 * @brief Header file with time management implementation - includes time measurements and delay handling.
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/

#pragma once

#include "CncConfig.h"

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
*****************************************************************************/

typedef volatile struct {
    bool enableFlag;
    uint32_t occurenceTimestamp;
} timeEvent_t;

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

/*************************************************************************************************
 *  @brief              Initialize the time management.                                         **
 *                                                                                              **
 *************************************************************************************************/
void SoftwareTimerInit(void);

/*************************************************************************************************
 *  @brief              Get global time calculated in FW.                                       **
 *                                                                                              **
 *  @return             Time from FW start [ms]                                                 **
 *                                                                                              **
 *************************************************************************************************/
uint32_t SoftwareTimerGetGlobalTime(void);

/*************************************************************************************************
 *  @brief              Trigger to count-down global timer - 1kHz.                              **
 *                                                                                              **
 *************************************************************************************************/
void SoftwareTimerTrigger(void);

/*************************************************************************************************
 *  @brief              Activate count-down timer: count-down time 0 - (2^31 - 1)ms.            **
 *                                                                                              **
 *  @param timeEvent    Selected event.                                                         **
 *  @param delayMs      Delay in 1ms resolution.                                                **
 *                                                                                              **
 *  @return             Time from FW start [ms]                                                 **
 *                                                                                              **
 *************************************************************************************************/
void SoftwareTimerEventActivateMs(timeEvent_t *timeEvent, uint32_t delayMs);

/*************************************************************************************************
 *  @brief              Check the passed time during timeout.                                   **
 *                                                                                              **
 *  @param timeEvent    Selected event.                                                         **
 *                                                                                              **
 *  @return             The passed time value.                                                  **
 *                                                                                              **
 *************************************************************************************************/
uint32_t SoftwareTimerEventPassedTime(timeEvent_t *timeEvent, uint32_t delayMs);

/*************************************************************************************************
 *  @brief              Activate event with constant delay from previous event - call first     **
 *                      SoftwareTimerEventActivateMs() to initialize internal data.             **
 *                                                                                              **
 *  @param timeEvent    Selected event.                                                         **
 *  @param delayMs      Delay in ms.                                                            **
 *                                                                                              **
 *************************************************************************************************/
void SoftwareTimerEventMoveMs(timeEvent_t *timeEvent, uint32_t delayMs);

/*************************************************************************************************
 *  @brief              Deactivation of event.                                                  **
 *                                                                                              **
 *  @param timeEvent    Selected event.                                                         **
 *                                                                                              **
 *************************************************************************************************/
void SoftwareTimerEventDeactivate(timeEvent_t *timeEvent);

/*************************************************************************************************
 *  @brief              Check if event is active.                                               **
 *                                                                                              **
 *  @param timeEvent    Selected event.                                                         **
 *                                                                                              **
 *  @return             True if event running, otherwise false.                                 **
 *                                                                                              **
 *************************************************************************************************/
bool SoftwareTimerIsEventPending(timeEvent_t *timeEvent);

/*************************************************************************************************
 *  @brief              Check if event is ready.                                                **
 *                                                                                              **
 *  @param timeEvent    Selected event.                                                         **
 *                                                                                              **
 *  @return             True if event ready, otherwise false.                                   **
 *                                                                                              **
 *************************************************************************************************/
bool SoftwareTimerIsEventReady(timeEvent_t *timeEvent);


/****************************** END OF FILE  *********************************/
