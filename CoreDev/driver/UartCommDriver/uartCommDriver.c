/*****************************************************************************
 * @file uartCommDriver.c
 *
 * @brief Source file for the UART COMM driver
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/

#include "driver/UartCommDriver/uartCommDriver.h"
#include "driver/AssertDebug/debug.h"

#include "main.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#define UART_COMM_TIMEOUT (100000U)

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
*****************************************************************************/

static bool sReceiveFlag = false;
static UART_HandleTypeDef *sUartHandler;
extern UART_HandleTypeDef huart3;

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

bool UartCommDriverInit(uint8_t *data, uint8_t length)
{
    ASSERT(data);
    ASSERT(length > 0);

    bool status = false;

    sUartHandler = &huart3;
    status = UartCommDriverReceiveIT(data, length);

    return status;
}

void UartCommDriverReceiveFlagClear(void)
{
    sReceiveFlag = false;
}

bool UartCommDriverGetReceiveFlagStatus(void)
{
    return sReceiveFlag;
}

bool UartCommDriverReceiveIT(uint8_t *data, uint8_t length)
{
    ASSERT(data);
    ASSERT(length > 0);


    if (HAL_UART_Receive_IT(sUartHandler, data, length) != HAL_OK) {
        return false;
    }
    return true;
}

bool UartCommDriverReceivePolling(uint8_t *data, uint8_t length)
{
    ASSERT(data);
    ASSERT(length > 0);

    if (HAL_UART_Receive(sUartHandler, data, length, UART_COMM_TIMEOUT) != HAL_OK) {
        return false;
    }
    return true;
}

bool UartCommDriverTransmit(uint8_t *data, uint8_t length)
{
    ASSERT(data);
    ASSERT(length > 0);

    if (HAL_UART_Transmit(sUartHandler, data, length, UART_COMM_TIMEOUT) != HAL_OK) {
        return false;
    }
    return true;
}

/*****************************************************************************
                           ST HAL LIBRARY RELATED
*****************************************************************************/

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    if (huart != sUartHandler) {
        return;
    }

    sReceiveFlag = true;
}

/****************************** END OF FILE  *********************************/
