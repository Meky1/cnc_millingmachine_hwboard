/*****************************************************************************
 * @file uartDriver.h
 *
 * @brief Header file for the UART driver
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/

#pragma once

#include "CncConfig.h"

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
*****************************************************************************/

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

/*************************************************************************************************
 *  @brief      Initialize the UART command driver.                                             **
 *                                                                                              **
 *  @return     True if device is initialized successfully, otherwise false.                    **
 *                                                                                              **
 *************************************************************************************************/
bool UartCommDriverInit(uint8_t *data, uint8_t length);

/*************************************************************************************************
 *  @brief      Clear the receive flag.                                                         **
 *                                                                                              **
 *************************************************************************************************/
void UartCommDriverReceiveFlagClear(void);

/*************************************************************************************************
 *  @brief      Get the receive flag status. The flag is raised when data has                   **
 *              been fully received                                                             **
 *                                                                                              **
 *  @return     True if flag raised, otherwise false.                                           **
 *                                                                                              **
 *************************************************************************************************/
bool UartCommDriverGetReceiveFlagStatus(void);

/*************************************************************************************************
 *  @brief          Initiate the UART receive protocol using interrupts.                        **
 *                                                                                              **
 *  @param data     Pointer to the data buffer, which should be filled.                         **
 *  @param length   Value describing how much data should be obtained via UART.                 **
 *                                                                                              **
 *  @return         True if transmission is initialized successfully, otherwise false.          **
 *                                                                                              **
 *************************************************************************************************/
bool UartCommDriverReceiveIT(uint8_t *data, uint8_t length);

/*************************************************************************************************
 *  @brief          Initiate the UART receive protocol in polling mode.                         **
 *                                                                                              **
 *  @param data     Pointer to the data buffer, which should be filled.                         **
 *  @param length   Value describing how much data should be obtained via UART.                 **
 *                                                                                              **
 *  @return         True if transmission is initialized successfully, otherwise false.          **
 *                                                                                              **
 *************************************************************************************************/
bool UartCommDriverReceivePolling(uint8_t *data, uint8_t length);

/*************************************************************************************************
 *  @brief          Transmit data via UART protocol.                                            **
 *                                                                                              **
 *  @param data     Pointer to the data buffer, which should be filled.                         **
 *  @param length   Value describing how much data should be sent via UART.                     **
 *                                                                                              **
 *  @return         True if data is sent successfully, otherwise false.                         **
 *                                                                                              **
 *************************************************************************************************/
bool UartCommDriverTransmit(uint8_t *data, uint8_t length);

/****************************** END OF FILE  *********************************/
