/*****************************************************************************
 *
 * @file mcuDriver.c
 *
 * @brief Abstraction layer for HAL STM32 core driver
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/

#include "driver/McuDriver/mcuDriver.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
 *****************************************************************************/

/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
******************************************************************************/

__STATIC_INLINE bool IsInInterrupt(void)
{
    return (SCB->ICSR & SCB_ICSR_VECTACTIVE_Msk) != 0;
}

__STATIC_INLINE bool IsInDebugMode(void)
{
    return ((CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk));
}

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

void McuDriverSystemReset(void)
{
    HAL_NVIC_SystemReset();
}

void McuDriverEnableIRQ(IRQn_Type IRQn)
{
    HAL_NVIC_EnableIRQ(IRQn);
}

void McuDriverDisableIRQ(IRQn_Type IRQn)
{
    HAL_NVIC_DisableIRQ(IRQn);
}

void McuDriverInsertBreakpoint(void)
{
    if (!IsInDebugMode()) {
        return;
    }

    __ASM volatile("BKPT #0");
}

void McuDriverSetPriority(IRQn_Type IRQn, uint32_t PreemptPriority, uint32_t SubPriority)
{
    HAL_NVIC_SetPriority(IRQn, PreemptPriority, SubPriority);
}

void McuDriverClearPendingIRQ(IRQn_Type IRQn)
{
    HAL_NVIC_ClearPendingIRQ(IRQn);
}

bool McuDriverIsInInterrupt(void)
{
    return IsInInterrupt();
}

/****************************** END OF FILE  *********************************/
