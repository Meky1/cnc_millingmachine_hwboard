/*****************************************************************************
 *
 * @file mcuDriver.h
 *
 * @brief Abstraction layer for HAL STM32 core driver
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/
#pragma once

#include "CncConfig.h"
#include "stm32f4xx_hal.h"

/*****************************************************************************
                     PUBLIC STRUCTS / ENUMS / VARIABLES
*****************************************************************************/
typedef enum {
    MCU_TIMER_SYSTEM_STATS = 0,
} timerId_t;

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
*****************************************************************************/

void McuDriverSystemReset(void);
void McuDriverEnableIRQ(IRQn_Type IRQn);
void McuDriverDisableIRQ(IRQn_Type IRQn);
void McuDriverSetPriority(IRQn_Type IRQn, uint32_t PreemptPriority, uint32_t SubPriority);
void McuDriverClearPendingIRQ(IRQn_Type IRQn);
bool McuDriverIsInInterrupt(void);
void McuDriverInsertBreakpoint(void);

/****************************** END OF FILE  *********************************/
