
/*****************************************************************************
 *
 * @file StepperMotor/stepperMotorsCommand.h
 *
 * @brief Header file containing all possible commands and fields for the L6480H stepper motor driver
 *
 * @author Marcin Czarnik
 * @date 22.07.2022
 * @version v1.0
 *
 ****************************************************************************/
#pragma once

#include "CncConfig.h"

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
 *****************************************************************************/

/******************************************************************************
                             BASIC MOTOR PARAMETERS
 ******************************************************************************/

/* Configuration for Motor X.
****************************************************************/
#define MOTOR_X_ACCELERATION (100U)   // [steps/s^2]
#define MOTOR_X_DECELERATION (2008U)  // [steps/s^2]
#define MOTOR_X_DEFAULT_SPEED (200U)  // [steps/s]

/* Configuration for Motor Y.
****************************************************************/
#define MOTOR_Y_ACCELERATION (100U)   // [steps/s^2]
#define MOTOR_Y_DECELERATION (2008U)  // [steps/s^2]
#define MOTOR_Y_DEFAULT_SPEED (200U)  // [steps/s]

/* Configuration for Motor Z.
****************************************************************/
#define MOTOR_Z_ACCELERATION (100U)   // [steps/s^2]
#define MOTOR_Z_DECELERATION (2008U)  // [steps/s^2]
#define MOTOR_Z_DEFAULT_SPEED (200U)  // [steps/s]

/******************************************************************************
                 CONSTANT PARAMETERS FROM THE L6480 DATASHEET
 ******************************************************************************/
#define CONSTANT_ROUND_UP (0.5L)                    // Round up value for the float->int conversion
#define CONSTANT_LENGTH_OF_ONE_TICK (0.000000250L)  // The length of a tick is 250ns
#define CONSTANT_SEARCH_SPEED_PARAM (67.108864L)    // LENGTH_OF_ONE_TICK * 2^28;
#define CONSTANT_MAX_SPEED_PARAM (0.065536L)        // LENGTH_OF_ONE_TICK * 2^18;
#define CONSTANT_MIN_SPEED_PARAM (4.194304L)        // LENGTH_OF_ONE_TICK * 2^24;
#define CONSTANT_ACC_DEC_PARAM (0.068719476736L)    // LENGTH_OF_ONE_TICK^2 * 2^40
#define CONSTANT_KVAL_PARAM (0.390625L)
#define CONSTANT_BEMF_SLOPE_PARAM (0.00156862745098L)
#define CONSTANT_KTHERM_PARAM (0.03125L)         // Resolution
#define CONSTANT_STALL_THRESHOLD_PARAM (31.25L)  // Voltage resolution of 31.25mV

/******************************************************************************
     CONSTANT MACROS FROM THE L6480 DATASHEET USED TO SET VARIOUS PARAMETERS
 ******************************************************************************/

/* @brief Speed conversion, range 0 to 15625 steps/s */
#define CONV_SPEED_SEARCH_STEPS_TO_PARAM(steps) ((uint32_t)(((steps)*CONSTANT_SEARCH_SPEED_PARAM) + CONSTANT_ROUND_UP))

/* @brief Max Speed conversion, range 15.25 to 15610 steps/s */
#define CONV_SPEED_DEFAULT_STEPS_TO_PARAM(steps) ((uint16_t)(((steps)*CONSTANT_MAX_SPEED_PARAM) + CONSTANT_ROUND_UP))

/* @brief Min Speed conversion, range 0 to 976.3 steps/s */
#define CONV_SPEED_MINIMAL_STEPS_TO_PARAM(steps) ((uint16_t)(((steps)*CONSTANT_MIN_SPEED_PARAM) + CONSTANT_ROUND_UP))

/* @brief Full Step Speed conversion, range 7.63 to 15625 steps/s */
#define CONV_SPEED_FULL_STEP_STEPS_TO_PARAM(steps) ((uint16_t)((steps)*CONSTANT_MAX_SPEED_PARAM))

/* @brief Intersect Speed conversion, range 0 to 3906 steps/s */
#define CONV_SPEED_INTERSECT_STEPS_TO_PARAM(steps) ((uint16_t)(((steps)*CONSTANT_MIN_SPEED_PARAM) + CONSTANT_ROUND_UP))

/* @brief Acc/Dec rates conversion, range 14.55 to 59590 steps/s2 */
#define CONV_ACC_DEC_STEPS_TO_PARAM(steps) ((uint16_t)(((steps)*CONSTANT_ACC_DEC_PARAM) + CONSTANT_ROUND_UP))

/* @brief BEMF compensation slopes, range 0 to 0.4% s/step */
#define CONV_BEMF_SLOPE_PERCENTAGE_TO_PARAM(percentage) ((uint8_t)(((percentage) / CONSTANT_BEMF_SLOPE_PARAM) + CONSTANT_ROUND_UP))

/* @brief KVAL conversions, range 0.4% to 99.6% */
#define CONV_KVAL_PERCENTAGE_TO_PARAM(percentage) ((uint8_t)(((percentage) / CONSTANT_KVAL_PARAM) + CONSTANT_ROUND_UP))

/* @brief K_THERM compensation conversion, range 1 to 1.46875 */
#define CONV_KTHERM_TO_PARAM(ktherm) ((uint8_t)(((ktherm - 1.0L) / CONSTANT_KTHERM_PARAM) + CONSTANT_ROUND_UP))

/* @brief Stall Threshold conversion, range 31.25mA to 4000mA */
#define CONV_STALL_THRESHOLD_TO_PARAM(stall) ((uint8_t)(((stall - CONSTANT_STALL_THRESHOLD_PARAM) / CONSTANT_STALL_THRESHOLD_PARAM) + CONSTANT_ROUND_UP))

/******************************************************************************
                            ADVANCED MOTOR PARAMETERS
 ******************************************************************************/

/* Configuration for Motor X.
****************************************************************/
#define REG_MOTOR_X_ABS_POS (0x00)
#define REG_MOTOR_X_EL_POS (0x00)
#define REG_MOTOR_X_MARK (0x00)
#define REG_MOTOR_X_ACCELERATION (CONV_ACC_DEC_STEPS_TO_PARAM(MOTOR_X_ACCELERATION))
#define REG_MOTOR_X_DECELERATION (CONV_ACC_DEC_STEPS_TO_PARAM(MOTOR_X_DECELERATION))
#define REG_MOTOR_X_MAXIMUM_SPEED (CONV_SPEED_DEFAULT_STEPS_TO_PARAM(MOTOR_X_DEFAULT_SPEED))
#define REG_MOTOR_X_MINIMAL_SPEED (0x00)
#define REG_MOTOR_X_FS_SPD (0x027)
#define REG_MOTOR_X_KVAL_HOLD (0x15)
#define REG_MOTOR_X_KVAL_RUN (0x48)
#define REG_MOTOR_X_KVAL_ACC (0x29)
#define REG_MOTOR_X_KVAL_DEC (0x29)
#define REG_MOTOR_X_INT_SPD (0x0408)
#define REG_MOTOR_X_ST_SLP (0x19)
#define REG_MOTOR_X_FN_SLP_ACC (0x29)
#define REG_MOTOR_X_FN_SLP_DEC (0x29)
#define REG_MOTOR_X_K_THERM (0xF)
#define REG_MOTOR_X_STALL_TH (0x00)
#define REG_MOTOR_X_OCD_TH (0x09)
#define REG_MOTOR_X_STEP_MODE (0x07)
#define REG_MOTOR_X_ALARM_EN (0x23)
#define REG_MOTOR_X_GATECFG1 (0b0000011101011111)
#define REG_MOTOR_X_GATECFG2 (0b00000001)
#define REG_MOTOR_X_CONFIG (0b0000001000100000)

/* Configuration for Motor Y.
****************************************************************/
#define REG_MOTOR_Y_ABS_POS (0x00)
#define REG_MOTOR_Y_EL_POS (0x00)
#define REG_MOTOR_Y_MARK (0x00)
#define REG_MOTOR_Y_ACCELERATION (CONV_ACC_DEC_STEPS_TO_PARAM(MOTOR_Y_ACCELERATION))
#define REG_MOTOR_Y_DECELERATION (CONV_ACC_DEC_STEPS_TO_PARAM(MOTOR_Y_DECELERATION))
#define REG_MOTOR_Y_MAXIMUM_SPEED (CONV_SPEED_DEFAULT_STEPS_TO_PARAM(MOTOR_Y_DEFAULT_SPEED))
#define REG_MOTOR_Y_MINIMAL_SPEED (0x00)
#define REG_MOTOR_Y_FS_SPD (0x027)
#define REG_MOTOR_Y_KVAL_HOLD (0x15)
#define REG_MOTOR_Y_KVAL_RUN (0x48)
#define REG_MOTOR_Y_KVAL_ACC (0x29)
#define REG_MOTOR_Y_KVAL_DEC (0x29)
#define REG_MOTOR_Y_INT_SPD (0x0408)
#define REG_MOTOR_Y_ST_SLP (0x19)
#define REG_MOTOR_Y_FN_SLP_ACC (0x29)
#define REG_MOTOR_Y_FN_SLP_DEC (0x29)
#define REG_MOTOR_Y_K_THERM (0xF)
#define REG_MOTOR_Y_STALL_TH (0x00)
#define REG_MOTOR_Y_OCD_TH (0x09)
#define REG_MOTOR_Y_STEP_MODE (0x07)
#define REG_MOTOR_Y_ALARM_EN (0x23)
#define REG_MOTOR_Y_GATECFG1 (0b0000011101011111)
#define REG_MOTOR_Y_GATECFG2 (0b00000001)
#define REG_MOTOR_Y_CONFIG (0b0000001000100000)

/* Configuration for Motor Z.
****************************************************************/
#define REG_MOTOR_Z_ABS_POS (0x00)
#define REG_MOTOR_Z_EL_POS (0x00)
#define REG_MOTOR_Z_MARK (0x00)
#define REG_MOTOR_Z_ACCELERATION (CONV_ACC_DEC_STEPS_TO_PARAM(MOTOR_Z_ACCELERATION))
#define REG_MOTOR_Z_DECELERATION (CONV_ACC_DEC_STEPS_TO_PARAM(MOTOR_Z_DECELERATION))
#define REG_MOTOR_Z_MAXIMUM_SPEED (CONV_SPEED_DEFAULT_STEPS_TO_PARAM(MOTOR_Z_DEFAULT_SPEED))
#define REG_MOTOR_Z_MINIMAL_SPEED (0x00)
#define REG_MOTOR_Z_FS_SPD (0x027)
#define REG_MOTOR_Z_KVAL_HOLD (0x15)
#define REG_MOTOR_Z_KVAL_RUN (0x48)
#define REG_MOTOR_Z_KVAL_ACC (0x29)
#define REG_MOTOR_Z_KVAL_DEC (0x29)
#define REG_MOTOR_Z_INT_SPD (0x0408)
#define REG_MOTOR_Z_ST_SLP (0x19)
#define REG_MOTOR_Z_FN_SLP_ACC (0x29)
#define REG_MOTOR_Z_FN_SLP_DEC (0x29)
#define REG_MOTOR_Z_K_THERM (0xF)
#define REG_MOTOR_Z_STALL_TH (0x00)
#define REG_MOTOR_Z_OCD_TH (0x09)
#define REG_MOTOR_Z_STEP_MODE (0x07)
#define REG_MOTOR_Z_ALARM_EN (0x23)
#define REG_MOTOR_Z_GATECFG1 (0b0000011101011111)
#define REG_MOTOR_Z_GATECFG2 (0b00000001)
#define REG_MOTOR_Z_CONFIG (0b0000001000100000)

/*****************************************************************************
                     PUBLIC STRUCTS / ENUMS / VARIABLES
 *****************************************************************************/

typedef enum {
    STEPPER_MOTOR_X = 0,
    STEPPER_MOTOR_Y,
    STEPPER_MOTOR_Z,
} motorNumber_t;

typedef enum { DRIVER_ENABLED = 0, DRIVER_DISABLED } motorDriverStatus_t;

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

/****************************** END OF FILE  *********************************/
