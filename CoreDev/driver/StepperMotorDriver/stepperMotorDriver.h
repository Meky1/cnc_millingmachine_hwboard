
/*****************************************************************************
 *
 * @file StepperMotorDriver/stepperMotorDriver.h
 *
 * @brief Header file for the low layer of the motor driver L6480H
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/
#pragma once

#include "CncConfig.h"

#include "driver/StepperMotorDriver/stepperMotorCommands.h"
#include "driver/StepperMotorDriver/stepperMotorConfig.h"

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
 *****************************************************************************/

#define NUMBER_OF_STEPPER_MOTORS (3U)

/*****************************************************************************
                     PUBLIC STRUCTS / ENUMS / VARIABLES
 *****************************************************************************/

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

/*************************************************************************************************
 *  @brief                  This function starts the encoder timers for all motors.             **
 *                                                                                              **
 *  @param motorNumber      The number of the motor in the motor array (check enum              **
 *                          motorNumber_t in stepperMotorCommands.h).                           **
 *                                                                                              **
 *  @return                 True if timers started successfully, otherwise false.               **
 *                                                                                              **
 *************************************************************************************************/
bool MotorDriverStartEncoderTimer(motorNumber_t motorNumber);

/*************************************************************************************************
 *  @brief                  This function is used to write a single byte to the driver          **
 *                          using SPI.                                                          **
 *                                                                                              **
 *  @param motorNumber      The number of the motor in the motor array (check enum              **
 *                          motorNumber_t in stepperMotorCommands.h).                           **
 *  @param byte             The byte to be sent to the L6480H driver.                           **
 *                                                                                              **
 *  @return                 True if byte sent successfully, otherwise false.                    **
 *                                                                                              **
 *************************************************************************************************/
bool MotorDriverWriteByte(motorNumber_t motorNumber, uint8_t byte);

/*************************************************************************************************
 *  @brief                  Read a number of bytes from the L6480H driver                       **
 *                                                                                              **
 *  @param motorNumber      The number of the motor in the motor array (check enum              **
 *                          motorNumber_t in stepperMotorCommands.h).                           **
 *  @param command          A command byte sent to the L6480H in order to get parameter value.  **
 *  @param readData         Pointer to the received data frame.                                 **
 *  @param numberOfBytes    Number of expected receive bytes.                                   **
 *                                                                                              **
 *  @return                 True if byte received successfully, otherwise false.                **
 *                                                                                              **
 *************************************************************************************************/
bool MotorDriverReadBytes(motorNumber_t motorNumber, uint8_t command, uint32_t *readData, uint8_t numberOfBytes);

/*************************************************************************************************
 *  @brief                  Read a number of bytes from the L6480H driver                       **
 *                                                                                              **
 *  @param motorNumber      The number of the motor in the motor array (check enum              **
 *                          motorNumber_t in stepperMotorCommands.h).                           **
 *  @param command          A command byte sent to the L6480H in order to get parameter value.  **
 *  @param readData         Pointer to the received data frame.                                 **
 *  @param numberOfBytes    Number of expected receive bytes.                                   **
 *                                                                                              **
 *  @return                 True if byte received successfully, otherwise false.                **
 *                                                                                              **
 *************************************************************************************************/
bool MotorDriverReadBytes(motorNumber_t motorNumber, uint8_t command, uint32_t *readData, uint8_t numberOfBytes);

/*************************************************************************************************
 *  @brief                  Enable or disable the L6480H motor driver.                          **
 *                                                                                              **
 *  @param motorNumber      The number of the motor in the motor array (check enum              **
 *                          motorNumber_t in stepperMotorCommands.h).                           **
 *  @param status           The enabling status (check enum motorDriverStatus_t).               **
 *                                                                                              **
 *************************************************************************************************/
void MotorDriverSetDriverStatus(motorNumber_t motorNumber, motorDriverStatus_t status);

/*************************************************************************************************
 *  @brief                  Read the busy pin state from the L6480H driver.                     **
 *                                                                                              **
 *  @param motorNumber      The number of the motor in the motor array (check enum              **
 *                          motorNumber_t in stepperMotorCommands.h).                           **
 *                                                                                              **
 *  @return                 True if pin is set, otherwise false.                                **
 *                                                                                              **
 *************************************************************************************************/
bool MotorDriverIsBusyPinSet(motorNumber_t motorNumber);

/*************************************************************************************************
 *  @brief                  Read the SW pin state from the L6480H driver.                       **
 *                                                                                              **
 *  @param motorNumber      The number of the motor in the motor array (check enum              **
 *                          motorNumber_t in stepperMotorCommands.h).                           **
 *                                                                                              **
 *  @return                 True if byte received successfully, otherwise false.                **
 *                                                                                              **
 *************************************************************************************************/
bool MotorDriverIsEndstopPinSet(motorNumber_t motorNumber);

/*************************************************************************************************
 *  @brief                  Read the error flag pin state from the L6480H driver.               **
 *                                                                                              **
 *  @param motorNumber      The number of the motor in the motor array (check enum              **
 *                          motorNumber_t in stepperMotorCommands.h).                           **
 *                                                                                              **
 *  @return                 True if byte received successfully, otherwise false.                **
 *                                                                                              **
 *************************************************************************************************/
bool MotorDriverIsErrorPinSet(motorNumber_t motorNumber);


/****************************** END OF FILE  *********************************/
