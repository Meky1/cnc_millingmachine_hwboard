
/*****************************************************************************
 *
 * @file StepperMotor/stepperMotorCommands.h
 *
 * @brief Header file containing all possible commands and fields for the L6480H stepper motor driver
 *
 * @author Marcin Czarnik
 * @date 22.07.2022
 * @version v1.0
 *
 ****************************************************************************/
#pragma once

#include "CncConfig.h"

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
 *****************************************************************************/

/*****************************************************************************
                     PUBLIC STRUCTS / ENUMS / VARIABLES
 *****************************************************************************/

/*************************************************************************************************
 *  @brief       Type definition of commands, that can be sent to the driver via SPI.           **
 *                                                                                              **
 *************************************************************************************************/
typedef enum {
    MOT_NOP = ((uint8_t)0x00),
    MOT_SET_PARAM = ((uint8_t)0x00),
    MOT_GET_PARAM = ((uint8_t)0x20),
    MOT_RUN = ((uint8_t)0x50),
    MOT_STEP_CLOCK = ((uint8_t)0x58),
    MOT_MOVE = ((uint8_t)0x40),
    MOT_GO_TO = ((uint8_t)0x60),
    MOT_GO_TO_DIR = ((uint8_t)0x68),
    MOT_GO_UNTIL = ((uint8_t)0x82),
    MOT_GO_UNTIL_ACT_CPY = ((uint8_t)0x8A),
    MOT_RELEASE_SW = ((uint8_t)0x92),
    MOT_GO_HOME = ((uint8_t)0x70),
    MOT_GO_MARK = ((uint8_t)0x78),
    MOT_RESET_POS = ((uint8_t)0xD8),
    MOT_RESET_DEVICE = ((uint8_t)0xC0),
    MOT_SOFT_STOP = ((uint8_t)0xB0),
    MOT_HARD_STOP = ((uint8_t)0xB8),
    MOT_SOFT_HIZ = ((uint8_t)0xA0),
    MOT_HARD_HIZ = ((uint8_t)0xA8),
    MOT_GET_STATUS = ((uint8_t)0xD0),
    MOT_RESERVED_CMD2 = ((uint8_t)0xEB),
    MOT_RESERVED_CMD1 = ((uint8_t)0xF8)
} motorCommands_t;

/*************************************************************************************************
 *  @brief       Type definition of driver/motor parameters that are possible to be             **
 *               set or get by using functions get_param() or set_param().                      **
 *                                                                                              **
 *************************************************************************************************/
typedef enum {
    MOT_ABS_POS = ((uint8_t)0x01),
    MOT_EL_POS = ((uint8_t)0x02),
    MOT_MARK = ((uint8_t)0x03),
    MOT_SPEED = ((uint8_t)0x04),
    MOT_ACC = ((uint8_t)0x05),
    MOT_DEC = ((uint8_t)0x06),
    MOT_MAX_SPEED = ((uint8_t)0x07),
    MOT_MIN_SPEED = ((uint8_t)0x08),
    MOT_FS_SPD = ((uint8_t)0x15),
    MOT_KVAL_HOLD = ((uint8_t)0x09),
    MOT_KVAL_RUN = ((uint8_t)0x0A),
    MOT_KVAL_ACC = ((uint8_t)0x0B),
    MOT_KVAL_DEC = ((uint8_t)0x0C),
    MOT_INT_SPD = ((uint8_t)0x0D),
    MOT_ST_SLP = ((uint8_t)0x0E),
    MOT_FN_SLP_ACC = ((uint8_t)0x0F),
    MOT_FN_SLP_DEC = ((uint8_t)0x10),
    MOT_K_THERM = ((uint8_t)0x11),
    MOT_ADC_OUT = ((uint8_t)0x12),
    MOT_OCD_TH = ((uint8_t)0x13),
    MOT_STALL_TH = ((uint8_t)0x14),
    MOT_STEP_MODE = ((uint8_t)0x16),
    MOT_ALARM_EN = ((uint8_t)0x17),
    MOT_GATECFG1 = ((uint8_t)0x18),
    MOT_GATECFG2 = ((uint8_t)0x19),
    MOT_CONFIG = ((uint8_t)0x1A),
    MOT_STATUS = ((uint8_t)0x1B),
} motorRegisters_t;

/*************************************************************************************************
 *  @brief        Type definition of microstepping modes.                                       **
 *                                                                                              **
 *************************************************************************************************/
typedef enum {
    MOT_STEP_SEL_1 = ((uint8_t)0x00),
    MOT_STEP_SEL_1_2 = ((uint8_t)0x01),
    MOT_STEP_SEL_1_4 = ((uint8_t)0x02),
    MOT_STEP_SEL_1_8 = ((uint8_t)0x03),
    MOT_STEP_SEL_1_16 = ((uint8_t)0x04),
    MOT_STEP_SEL_1_32 = ((uint8_t)0x05),
    MOT_STEP_SEL_1_64 = ((uint8_t)0x06),
    MOT_STEP_SEL_1_128 = ((uint8_t)0x07)
} motorStepSel_t;

/*************************************************************************************************
 *  @brief        Type definition of configurations available.                                  **
 *                                                                                              **
 *************************************************************************************************/
typedef enum {
    MOT_CONFIG_OSC_SEL = ((uint16_t)0x0007),
    MOT_CONFIG_EXT_CLK = ((uint16_t)0x0008),
    MOT_CONFIG_SW_MODE = ((uint16_t)0x0010),
    MOT_CONFIG_EN_VSCOMP = ((uint16_t)0x0020),
    MOT_CONFIG_OC_SD = ((uint16_t)0x0080),
    MOT_CONFIG_UVLOVAL = ((uint16_t)0x0100),
    MOT_CONFIG_VCCVAL = ((uint16_t)0x0200),
    MOT_CONFIG_F_PWM_DEC = ((uint16_t)0x1C00),
    MOT_CONFIG_F_PWM_INT = ((uint16_t)0xE000)
} motorConfigMasks_t;

/*************************************************************************************************
 *  @brief       Type definition of possible thermal statuses/failures.                         **
 *                                                                                              **
 *************************************************************************************************/
typedef enum {
    MOT_THERM_NORMAL = ((uint8_t)0x00),
    MOT_THERM_WARNING = ((uint8_t)0x01),
    MOT_THERM_BRIDGE_SHUTDOWN = ((uint8_t)0x02),
    MOT_THERM_DEVICE_SHUTDOWN = ((uint8_t)0x03),
} motorThermalStatus_t;

/*************************************************************************************************
 *  @brief        Type definition of both directions.                                           **
 *                                                                                              **
 *************************************************************************************************/
typedef enum { CNTR_CLOCKWISE = ((uint8_t)0x00), CLOCKWISE = ((uint8_t)0x01) } motorDirection_t;

/*************************************************************************************************
 *  @brief        Type definition of actions. In GoUntilEndstop(ACT, DIR, SPD) command action   **
 *                tells whether the ABS_POS is going to be reset (ACT = 0) or the ABS_POS       **
 *                register value is going to be copied to the MARK register (ACT = 1).          **
 *                                                                                              **
 *************************************************************************************************/
typedef enum { ACTION_RESET = ((uint8_t)0x00), ACTION_COPY = ((uint8_t)0x08) } motorAction_t;

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

/****************************** END OF FILE  *********************************/
