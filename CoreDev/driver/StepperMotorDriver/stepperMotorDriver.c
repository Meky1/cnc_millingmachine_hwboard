
/*****************************************************************************
 *
 * @file StepperMotorDriver/stepperMotorDriver.c
 *
 * @brief Source file for the low layer of the motor driver L6480H
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/

#include "driver/StepperMotorDriver/stepperMotorDriver.h"
#include "driver/AssertDebug/debug.h"
#include "driver/McuDriver/mcuDriver.h"
#include "main.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#ifdef CFG_DEBUG_MODULE_NAME
#undef CFG_DEBUG_MODULE_NAME
#endif

#define CFG_DEBUG_MODULE_NAME "[STEP_MOTOR] "

#define MAX_NUMBER_OF_BYTES (3U)
#define TRANSMISSION_TIMEOUT (100U)
#define BYTE_SIZE (8U)

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
 *****************************************************************************/

/* Motor timer handling structure for encoder support.
****************************************************************/
typedef struct {
    TIM_HandleTypeDef *timHandler;
    IRQn_Type interruptNumberDefinition;
} motorTimerHandler_t;

/* Motor HW pinout structure.
****************************************************************/
typedef struct {

    /* Chip Select.
    ********************************/
    GPIO_TypeDef *_MDx_CS_GPIOx;
    uint16_t _MDx_CS_Pin;

    /* Busy pin.
    ********************************/
    GPIO_TypeDef *_MDx_BUSY_GPIOx;
    uint16_t _MDx_BUSY_Pin;

    /* Endstop pin.
    ********************************/
    GPIO_TypeDef *MDx_SW_GPIOx;
    uint16_t MDx_SW_Pin;

    /* Error flag pin.
    ********************************/
    GPIO_TypeDef *_MDx_FLAG_GPIOx;
    uint16_t _MDx_FLAG_Pin;

    /* Enable/disable motor driver.
    ********************************/
    GPIO_TypeDef *_MDx_STBY_RST_GPIOx;
    uint16_t _MDx_STBY_RST_Pin;

    /* Encoder timer structure.
    ********************************/
    motorTimerHandler_t MDx_ENC_TIM_HANDLE;

} motorPinoutStruct_t;

/* Motor timer and SPI handlers.
****************************************************************/
extern SPI_HandleTypeDef hspi4;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;

// clang-format off

/* Motor drivers pinout initialization.
****************************************************************/
static motorPinoutStruct_t sMotorArrayPinout[NUMBER_OF_STEPPER_MOTORS] = {
    { 
        ._MDx_CS_GPIOx = _MD_MOTOR_X_CS_GPIO_Port, ._MDx_CS_Pin = _MD_MOTOR_X_CS_Pin,
        ._MDx_BUSY_GPIOx = _MD_MOTOR_X_BUSY_GPIO_Port, ._MDx_BUSY_Pin = _MD_MOTOR_X_BUSY_Pin,
        .MDx_SW_GPIOx = MD_MOTOR_X_SW_GPIO_Port, .MDx_SW_Pin = MD_MOTOR_X_SW_Pin,
        ._MDx_FLAG_GPIOx = _MD_MOTOR_X_FLAG_GPIO_Port, ._MDx_FLAG_Pin = _MD_MOTOR_X_FLAG_Pin,
        ._MDx_STBY_RST_GPIOx = _MD_MOTOR_X_STBY_RST_GPIO_Port, ._MDx_STBY_RST_Pin = _MD_MOTOR_X_STBY_RST_Pin,
        {
            .timHandler = &htim2,
            .interruptNumberDefinition = TIM2_IRQn
        }
    },
    {   ._MDx_CS_GPIOx = _MD_MOTOR_Y_CS_GPIO_Port, ._MDx_CS_Pin = _MD_MOTOR_Y_CS_Pin,
        ._MDx_BUSY_GPIOx = _MD_MOTOR_Y_BUSY_GPIO_Port, ._MDx_BUSY_Pin = _MD_MOTOR_Y_BUSY_Pin,
        .MDx_SW_GPIOx = MD_MOTOR_Y_SW_GPIO_Port, .MDx_SW_Pin = MD_MOTOR_Y_SW_Pin,
        ._MDx_FLAG_GPIOx = _MD_MOTOR_Y_FLAG_GPIO_Port, ._MDx_FLAG_Pin = _MD_MOTOR_Y_FLAG_Pin,
        ._MDx_STBY_RST_GPIOx = _MD_MOTOR_Y_STBY_RST_GPIO_Port, ._MDx_STBY_RST_Pin = _MD_MOTOR_Y_STBY_RST_Pin, 
        {
            .timHandler = &htim3,
            .interruptNumberDefinition = TIM3_IRQn
        }
    },
    {   ._MDx_CS_GPIOx = _MD_MOTOR_Z_CS_GPIO_Port, ._MDx_CS_Pin = _MD_MOTOR_Z_CS_Pin,
        ._MDx_BUSY_GPIOx = _MD_MOTOR_Z_BUSY_GPIO_Port, ._MDx_BUSY_Pin = _MD_MOTOR_Z_BUSY_Pin,
        .MDx_SW_GPIOx = MD_MOTOR_Z_SW_GPIO_Port, .MDx_SW_Pin = MD_MOTOR_Z_SW_Pin,
        ._MDx_FLAG_GPIOx = _MD_MOTOR_Z_FLAG_GPIO_Port, ._MDx_FLAG_Pin = _MD_MOTOR_Z_FLAG_Pin,
        ._MDx_STBY_RST_GPIOx = _MD_MOTOR_Z_STBY_RST_GPIO_Port, ._MDx_STBY_RST_Pin = _MD_MOTOR_Z_STBY_RST_Pin,
        {
            .timHandler = &htim4,
            .interruptNumberDefinition = TIM4_IRQn
        }
    }
};
// clang-format on

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
 *****************************************************************************/

bool MotorDriverStartEncoderTimer(motorNumber_t motorNumber)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    McuDriverEnableIRQ(sMotorArrayPinout[motorNumber].MDx_ENC_TIM_HANDLE.interruptNumberDefinition);
    if (HAL_TIM_Base_Start_IT(sMotorArrayPinout[motorNumber].MDx_ENC_TIM_HANDLE.timHandler) != HAL_OK) {
        return false;
    }
    return true;
}

bool MotorDriverWriteByte(motorNumber_t motorNumber, uint8_t byte)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    /* Transmit data via SPI; setting the appropriate chip select pin.
    ******************************************************************/
    HAL_GPIO_WritePin(sMotorArrayPinout[motorNumber]._MDx_CS_GPIOx, sMotorArrayPinout[motorNumber]._MDx_CS_Pin, GPIO_PIN_RESET);
    HAL_StatusTypeDef status = HAL_SPI_Transmit(&hspi4, &byte, 1, TRANSMISSION_TIMEOUT);
    HAL_GPIO_WritePin(sMotorArrayPinout[motorNumber]._MDx_CS_GPIOx, sMotorArrayPinout[motorNumber]._MDx_CS_Pin, GPIO_PIN_SET);

    if (status != HAL_OK) {
        return false;
    }
    return true;
}

bool MotorDriverReadBytes(motorNumber_t motorNumber, uint8_t command, uint32_t *readData, uint8_t numberOfBytes)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));
    ASSERT(numberOfBytes <= MAX_NUMBER_OF_BYTES);
    ASSERT(readData);

    bool status = true;
    *readData = 0;
    uint8_t byteReceivedArray[MAX_NUMBER_OF_BYTES] = { 0, 0, 0 };

    /* Write the read data command byte.
    ****************************************************************/
    status &= MotorDriverWriteByte(motorNumber, command);

    /* Receive data via SPI; setting the appropriate chip select pin.
    *****************************************************************/
    for (int i = 0; i < numberOfBytes; i++) {
        HAL_GPIO_WritePin(sMotorArrayPinout[motorNumber]._MDx_CS_GPIOx, sMotorArrayPinout[motorNumber]._MDx_CS_Pin, GPIO_PIN_RESET);
        HAL_StatusTypeDef receiveStatus = HAL_SPI_Receive(&hspi4, byteReceivedArray + i, 1, TRANSMISSION_TIMEOUT);
        HAL_GPIO_WritePin(sMotorArrayPinout[motorNumber]._MDx_CS_GPIOx, sMotorArrayPinout[motorNumber]._MDx_CS_Pin, GPIO_PIN_SET);

        if (receiveStatus != HAL_OK) {
            status = false;
        }
    }

    /* Process the data and cast it to a uint32_t value.
    *****************************************************************/
    for (uint8_t i = 0; i < numberOfBytes; i++) {
        *readData |= (uint32_t)byteReceivedArray[i] << (numberOfBytes - i - 1u) * BYTE_SIZE;
    }

    return status;
}

void MotorDriverSetDriverStatus(motorNumber_t motorNumber, motorDriverStatus_t enableStatus)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));
    ASSERT((enableStatus == DRIVER_ENABLED) || (enableStatus == DRIVER_DISABLED));

    HAL_GPIO_WritePin(sMotorArrayPinout[motorNumber]._MDx_STBY_RST_GPIOx, sMotorArrayPinout[motorNumber]._MDx_STBY_RST_Pin, (GPIO_PinState)enableStatus);
}

bool MotorDriverIsBusyPinSet(motorNumber_t motorNumber)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    /* Read GPIO Busy pin:
       GPIO_PIN_SET - motor is busy - a command is being executed.
       GPIO_PIN_RESET - motor is idle and waiting for a new command.
    *****************************************************************/
    if (HAL_GPIO_ReadPin(sMotorArrayPinout[motorNumber]._MDx_BUSY_GPIOx, sMotorArrayPinout[motorNumber]._MDx_BUSY_Pin) == GPIO_PIN_SET) {
        return true;
    }
    return false;
}

bool MotorDriverIsEndstopPinSet(motorNumber_t motorNumber)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    /* Read GPIO SW pin:
       GPIO_PIN_SET - endstop is not reached, motor is not at home position.
       GPIO_PIN_RESET - endstop is reached, motor is at home position.
    *****************************************************************/
    if (HAL_GPIO_ReadPin(sMotorArrayPinout[motorNumber].MDx_SW_GPIOx, sMotorArrayPinout[motorNumber].MDx_SW_Pin) == GPIO_PIN_SET) {
        return false;
    }
    return true;
}

bool MotorDriverIsErrorPinSet(motorNumber_t motorNumber)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    /* Read GPIO Error Flag pin:
       GPIO_PIN_SET - Error Flag is set, meaning there are no reported faults.
       GPIO_PIN_RESET - Error Flag is not set, meaning there are reported faults.
    *****************************************************************/
    if (HAL_GPIO_ReadPin(sMotorArrayPinout[motorNumber]._MDx_FLAG_GPIOx, sMotorArrayPinout[motorNumber]._MDx_FLAG_Pin) == GPIO_PIN_RESET) {
        return true;
    }
    return false;
}