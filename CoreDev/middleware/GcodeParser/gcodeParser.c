/*****************************************************************************
 *
 * @file middleware/GcodeParser/gcodeParser.c
 *
 * @brief Source file for the gcode parser used to interpret string lines in order to obtain commands with it's arguments.
 *
 * @author Marcin Czarnik
 * @date 03.08.2022
 * @version v1.0
 *
 ****************************************************************************/

#include "middleware/GcodeParser/gcodeParser.h"
#include "middleware/CommandReception/commandReception.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#define CFG_DEBUG_MODULE_NAME "[GCODE_PARSE]"

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
*****************************************************************************/

/*****************************************************************************
                         PRIVATE FUNCTION DECLARATION
*****************************************************************************/

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

systemErrorCodes_t GcodeParserLineParse(void)
{
    const char *gcodeLine = CommandReceptionGetGcodeLine();

    return GCODE_PARSE_OK;
}


/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
******************************************************************************/