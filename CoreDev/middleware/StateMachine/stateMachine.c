/*****************************************************************************
 *
 * @file stateMachine.c
 *
 * @brief State machine
 *
 * @author Marcin Czarnik
 * @date 20.11.2022
 * @version v1.0
 *
 ****************************************************************************/

#include "middleware/StateMachine/stateMachine.h"
#include "driver/AssertDebug/debug.h"
#include "driver/TimeBaseDriver/timeBaseDriver.h"

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

bool StateMachineInit(stateMachine_t *stateMachine, const char *name, state_t *states, uint32_t statesLen)
{
    ASSERT(stateMachine);
    ASSERT(name);
    ASSERT(states);
    ASSERT(statesLen > 0);

    for (uint32_t i = 0; i < statesLen; ++i) {
        ASSERT(states[i].name);
        ASSERT(states[i].process);
    }

    stateMachine->name = name;
    stateMachine->states = states;
    stateMachine->statesLen = statesLen;
    stateMachine->currentStateId = 0;

    StateMachineChangeState(stateMachine, 0);

    return true;
}

void StateMachineChangeState(stateMachine_t *stateMachine, uint32_t stateId)
{
    ASSERT(stateMachine);
    ASSERT(stateId < stateMachine->statesLen);

    stateMachine->currentStateStartTime = TimeBaseDriverGetTick();

    if (stateId != stateMachine->currentStateId) {
        stateMachine->currentStateId = stateId;
    }
}

void StateMachineProcess(stateMachine_t *stateMachine)
{
    ASSERT(stateMachine);

    uint32_t timeoutVal = stateMachine->states[stateMachine->currentStateId].timeout;

    if (timeoutVal > 0 && TimeBaseDriverGetTick() - stateMachine->currentStateStartTime > timeoutVal) {
        StateMachineChangeState(stateMachine, stateMachine->states[stateMachine->currentStateId].onTimeoutId);
    }

    stateResult_t result = stateMachine->states[stateMachine->currentStateId].process();

    switch (result) {
    case STATE_MACHINE_OK:
        break;
    case STATE_MACHINE_ERROR:
        StateMachineChangeState(stateMachine, stateMachine->states[stateMachine->currentStateId].onErrorId);
        break;
    case STATE_MACHINE_NEXT_STATE:
        if (stateMachine->currentStateId < stateMachine->statesLen - 1) {
            StateMachineChangeState(stateMachine, stateMachine->currentStateId + 1);
        }
        break;
    }
}

uint32_t StateMachineGetCurrentStateId(stateMachine_t *stateMachine)
{
    ASSERT(stateMachine);

    return stateMachine->currentStateId;
}

const char *StateMachineGetCurrentStateName(stateMachine_t *stateMachine)
{
    ASSERT(stateMachine);

    return stateMachine->states[stateMachine->currentStateId].name;
}
