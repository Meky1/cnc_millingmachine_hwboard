/*****************************************************************************
 *
 * @file StateMachine/stateMachine.h
 *
 * @brief General purpose state machine containing a built-in timeout system.
 *
 * @author Marcin Czarnik
 * @date 20.11.2022
 * @version v1.0
 *
 ****************************************************************************/
#pragma once

#include "CncConfig.h"

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
*****************************************************************************/

/** @brief State return codes */
typedef enum {
    STATE_MACHINE_OK = 0,
    STATE_MACHINE_ERROR,       // Request switch to error state
    STATE_MACHINE_NEXT_STATE,  // Request switch to next state
} stateResult_t;

/** @brief State instance */
typedef struct {
    const char *name;            // State name
    uint32_t timeout;            // State timeout (0 = disabled)
    stateResult_t (*process)();  // State process
    uint32_t onErrorId;          // State to change to if error occurs
    uint32_t onTimeoutId;        // State to change if timeout occurs
} state_t;

/** @brief State machine instance */
typedef struct {
    const char *name;                // State machine name
    state_t *states;                 // List of states
    uint32_t statesLen;              // Length of the list of states
    uint32_t currentStateId;         // ID of the current state
    uint32_t currentStateStartTime;  // Start time of current state
} stateMachine_t;

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

/*************************************************************************************************
 *  @brief                  Initialize the state machine.                                       **
 *                                                                                              **
 *  @param stateMachine     The state machine instance.                                         **
 *  @param name             The name of the state machine instance.                             **
 *  @param states           The state structure containing the ID of each state.                **
 *  @param statesLen        The length of the list of states.                                   **
 *                                                                                              **
 *  @return                 True if initialized successfully, otherwise false.                  **
 *                                                                                              **
 *************************************************************************************************/
bool StateMachineInit(stateMachine_t *stateMachine, const char *name, state_t *states, uint32_t statesLen);

/*************************************************************************************************
 *  @brief                  Change state of the state machine.                                  **
 *                                                                                              **
 *  @param stateMachine     The state machine instance.                                         **
 *  @param stateId          The ID of the state to change to.                                   **
 *                                                                                              **                                                                                              **
 *************************************************************************************************/
void StateMachineChangeState(stateMachine_t *stateMachine, uint32_t stateId);

/*************************************************************************************************
 *  @brief                  Execute state machine process. Checks for timeout, then executes    **
 *                          the process of current state. May change state in case of timeout   **
 *                          or based on state process return status.                            **
 *                                                                                              **
 *  @param stateMachine     The state machine instance.                                         **
 *                                                                                              **                                                                                              **
 *************************************************************************************************/
void StateMachineProcess(stateMachine_t *stateMachine);

/*************************************************************************************************
 *  @brief                  Get ID of the current state.                                        **
 *                                                                                              **
 *  @param stateMachine     The state machine instance.                                         **
 *                                                                                              **
 *  @return                 The current state ID.                                               **
 *                                                                                              **
 *************************************************************************************************/
uint32_t StateMachineGetCurrentStateId(stateMachine_t *stateMachine);

/*************************************************************************************************
 *  @brief                  Get ID of the current state.                                        **
 *                                                                                              **
 *  @param stateMachine     The state machine instance.                                         **
 *                                                                                              **
 *  @return                 The current state name.                                             **
 *                                                                                              **
 *************************************************************************************************/
const char *StateMachineGetCurrentStateName(stateMachine_t *stateMachine);
