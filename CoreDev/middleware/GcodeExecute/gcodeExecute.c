/*****************************************************************************
 *
 * @file middleware/GcodeExecute/gcodeExecute.c
 *
 * @brief Source file for the gcode parser used to interpret string lines in order to obtain commands with it's arguments.
 *
 * @author Marcin Czarnik
 * @date 03.08.2022
 * @version v1.0
 *
 ****************************************************************************/

#include "middleware/GcodeExecute/gcodeExecute.h"
#include "driver/AssertDebug/debug.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#ifdef CFG_DEBUG_MODULE_NAME
#undef CFG_DEBUG_MODULE_NAME
#endif

#define CFG_DEBUG_MODULE_NAME "[GCODE_EXE]"

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
*****************************************************************************/

/*****************************************************************************
                         PRIVATE FUNCTION DECLARATION
*****************************************************************************/

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

systemErrorCodes_t GcodeExecuteCommand(char *gcodeLine)
{
    ASSERT(gcodeLine);

    return COMMAND_DATA_OK;
}

/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
******************************************************************************/
