/*****************************************************************************
 *
 * @file driver/GcodeExecute/gcodeExecute.h
 *
 * @brief Execution code of the acquired GCode data line.
 *
 * @author Marcin Czarnik
 * @date 11.11.2022
 * @version v1.0
 *
 ****************************************************************************/

#pragma once

#include "CncConfig.h"

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
*****************************************************************************/

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
*****************************************************************************/

/*************************************************************************************************
 *  @brief              Execute the GCode command line.                                         **
 *                                                                                              **
 *  @param gcodeLine:   The pointer to the buffer containing the command line.                  **
 *                                                                                              **
 *  @return             The error code of the operation (range 0xYYY - 0xZZZ).                  **
 *                      For more information check systemErrorCodes_t enum in "CncConfig.h".    **
 *                                                                                              **
 *************************************************************************************************/
systemErrorCodes_t GcodeExecuteCommand(char *gcodeLine);
