
/*****************************************************************************
 *
 * @file StepperMotor/StepperMotor.h
 *
 * @brief Header file for the low layer of the motor driver L6480H
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/
#pragma once

#include "CncConfig.h"
#include "driver/StepperMotorDriver/stepperMotorCommands.h"
#include "driver/StepperMotorDriver/stepperMotorConfig.h"

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
 *****************************************************************************/

/*****************************************************************************
                     PUBLIC STRUCTS / ENUMS / VARIABLES
 *****************************************************************************/

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

/*************************************************************************************************
 *  @brief      Initialize the stepper motor driver.                                            **
 *                                                                                              **
 *  @return     True if initialization successful, otherwise false.                             **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorInit(void);

/*************************************************************************************************
 *  @brief      Deinitialize the stepper motor driver.                                          **
 *                                                                                              **
 *  @return     True if deinitialization successful, otherwise false.                           **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorDeinit(void);

/*************************************************************************************************
 *  @brief              Wait until command is done.                                             **
 *                                                                                              **
 *  @param motorNumber  The number of the motor in the motor array                              **
 *                      (check enum motorNumber_t in stepperMotorCommands.h).                   **
 *                                                                                              **
 *  @return             True if device is ready for a new command, otherwise false.             **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorIsCommandDone(motorNumber_t motorNumber);

/*************************************************************************************************
 *  @brief              Check the state of the endstop (if the motor is position in the range   **
 *                      of the photocoupler).                                                   **
 *                                                                                              **
 *  @param motorNumber  The number of the motor in the motor array                              **
 *                      (check enum motorNumber_t in stepperMotorCommands.h).                   **
 *                                                                                              **
 *  @return             True if endstop is in low state, otherwise false.                       **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorIsEndstopReached(motorNumber_t motorNumber);

/*************************************************************************************************
 *  @brief              Check if any error appeared.                                            **
 *                                                                                              **
 *  @param motorNumber  The number of the motor in the motor array (check enum motorNumber_t    **
 *                      in stepperMotorCommands.h).                                             **
 *                                                                                              **
 *  @return             True if error detected, otherwise false.                                **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorIsErrorDetected(motorNumber_t motorNumber);

/*************************************************************************************************
 *  @brief              Reset the status register.                                              **
 *                                                                                              **
 *  @param motorNumber  The number of the motor in the motor array (check enum motorNumber_t    **
 *                      in stepperMotorCommands.h).                                             **
 *                                                                                              **
 *  @return             True if command executed successfully, otherwise false.                 **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorResetStatusRegister(motorNumber_t motorNumber);

/*************************************************************************************************
 *  @brief              Print all parametrs to SWO.                                             **
 *                                                                                              **
 *  @param motorNumber  The number of the motor in the motor array (check enum motorNumber_t    **
 *                      in stepperMotorCommands.h).                                             **
 *                                                                                              **
 *  @return             True if command executed successfully, otherwise false.                 **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorPrintAllParameters(motorNumber_t motorNumber);

/*************************************************************************************************
 *  @brief              Stop the motor with deceleration.                                       **
 *                                                                                              **
 *  @param motorNumber  The number of the motor in the motor array (check enum motorNumber_t    **
 *                      in stepperMotorCommands.h).                                             **
 *                                                                                              **
 *  @return             True if command executed successfully, otherwise false.                 **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorSoftStop(motorNumber_t motorNumber);

/*************************************************************************************************
 *  @brief              Stop the motor without deceleration.                                    **
 *                                                                                              **
 *  @param motorNumber  The number of the motor in the motor array (check enum motorNumber_t    **
 *                      in stepperMotorCommands.h).                                             **
 *                                                                                              **
 *  @return             True if command executed successfully, otherwise false.                 **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorHardStop(motorNumber_t motorNumber);

/*************************************************************************************************
 *  @brief              Set high impedance state after motor's deceleration and stop.           **
 *                                                                                              **
 *  @param motorNumber  The number of the motor in the motor array (check enum motorNumber_t    **
 *                      in stepperMotorCommands.h).                                             **
 *                                                                                              **
 *  @return             True if command executed successfully, otherwise false.                 **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorSoftHiZ(motorNumber_t motorNumber);

/*************************************************************************************************
 *  @brief              Set high impedance state after motor's hard stop.                       **
 *                                                                                              **
 *  @param motorNumber  The number of the motor in the motor array (check enum motorNumber_t    **
 *                      in stepperMotorCommands.h).                                             **
 *                                                                                              **
 *  @return             True if command executed successfully, otherwise false.                 **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorHardHiZ(motorNumber_t motorNumber);

/*************************************************************************************************
 *  @brief              Release the SW pin - motor is no longer blocked by the photocoupler.    **
 *                                                                                              **
 *  @param motorNumber  The number of the motor in the motor array (check enum motorNumber_t    **
 *                      in stepperMotorCommands.h).                                             **
 *  @param action       Check MOT_Action_TypeDef enum declaration in MotorDriver.h              **
 *  @param direction    Move clockwise or counterclockwise.                                     **
 *                                                                                              **
 *  @return             True if procedure executed successfully, otherwise false.               **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorReleaseSW(motorNumber_t motorNumber, motorAction_t action, motorDirection_t direction);

/*************************************************************************************************
 *  @brief                  Sets the microstepping mode of the motor.                           **
 *                                                                                              **
 *  @param motorNumber      The number of the motor in the motor array (check enum              **
 *                          motorNumber_t in stepperMotorCommands.h).                           **
 *  @param stepResolution   Step resolution (check MOT_STEP_SEL_TypeDef enum in MotorDriver.h)  **
 *                                                                                              **
 *  @return                 True if procedure executed successfully, otherwise false.           **
 *                                                                                              **
 **************************************************************************************************/
bool StepperMotorSetMicrosteppingMode(motorNumber_t motorNumber, motorStepSel_t stepResolution);

/*************************************************************************************************
 *  @brief                  Sets the acceleration and deceleration values,                      **
 *                          range 14.55 to 59590 steps/s2.                                      **
 *                                                                                              **
 *  @param motorNumber      The number of the motor in the motor array (check enum              **
 *                          motorNumber_t in stepperMotorCommands.h).                           **
 *  @param acceleration     Acceleration value (in steps/s2).                                   **
 *  @param deceleration     Deceleration value (in steps/s2).                                   **
 *                                                                                              **
 *  @return                 True if procedure executed successfully, otherwise false.           **
 *                                                                                              **
 **************************************************************************************************/
bool StepperMotorSetAccelerationDecelaration(motorNumber_t motorNumber, uint16_t acceleration, uint16_t deceleration);

/*************************************************************************************************
 *  @brief              Sets the maximum speed, range 15.25 to 15610 steps/s.                   **
 *                                                                                              **
 *  @param motorNumber  The number of the motor in the motor array (check enum                  **
 *                      motorNumber_t in stepperMotorCommands.h).                               **
 *  @param speed        The speed value.                                                        **
 *                                                                                              **
 *  @return             True if procedure executed successfully, otherwise false.               **
 *                                                                                              **
 **************************************************************************************************/
bool StepperMotorSetMaximumSpeed(motorNumber_t motorNumber, uint32_t speed);

/*************************************************************************************************
 *  @brief                      Sets the absolute position of the motor.                        **
 *                                                                                              **
 *  @param motorNumber          The number of the motor in the motor array (check enum          **
 *                              motorNumber_t in stepperMotorCommands.h).                       **
 *  @param absolutePosition     The absolute position of the motor.                             **
 *                                                                                              **
 *  @return                     True if procedure executed successfully, otherwise false.       **
 *                                                                                              **
 **************************************************************************************************/
bool StepperMotorSetAbsolutePosition(motorNumber_t motorNumber, uint32_t absolutePosition);

/*************************************************************************************************
 *  @brief                      Sets the threshold overcurrent threshold value.                 **
 *                                                                                              **
 *  @param motorNumber          The number of the motor in the motor array (check enum          **
 *                              motorNumber_t in stepperMotorCommands.h).                       **
 *  @param maximumThreshold     The maximum overcurrent threshold value.                        **
 *                                                                                              **
 *  @return                     True if procedure executed successfully, otherwise false.       **
 *                                                                                              **
 **************************************************************************************************/
bool StepperMotorSetOvercurrentThreshold(motorNumber_t motorNumber, uint8_t maximumThreshold);

/*************************************************************************************************
 *  @brief                      Sets the threshold stall value.                                 **
 *                                                                                              **
 *  @param motorNumber          The number of the motor in the motor array (check enum          **
 *                              motorNumber_t in stepperMotorCommands.h).                       **
 *  @param maximumThreshold     The maximum stall threshold value.                              **
 *                                                                                              **
 *  @return                     True if procedure executed successfully, otherwise false.       **
 *                                                                                              **
 **************************************************************************************************/
bool StepperMotorSetStallThreshold(motorNumber_t motorNumber, uint8_t maximumThreshold);

/*************************************************************************************************
 *  @brief                  Sets the thermal resistance wire compensation.                      **
 *                                                                                              **
 *  @param motorNumber      The number of the motor in the motor array (check enum              **
 *                          motorNumber_t in stepperMotorCommands.h).                           **
 *  @param kthermValue      The compensation value.                                             **
 *                                                                                              **
 *  @return                 True if procedure executed successfully, otherwise false.           **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorSetKTherm(motorNumber_t motorNumber, uint8_t kthermValue);

/*************************************************************************************************
 *  @brief                  Sets the Kval run value.                                            **
 *                                                                                              **
 *  @param motorNumber      The number of the motor in the motor array (check enum              **
 *                          motorNumber_t in stepperMotorCommands.h).                           **
 *  @param maxThreshold     The kval value.                                                     **
 *                                                                                              **
 *  @return                 True if procedure executed successfully, otherwise false.           **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorSetKvalRun(motorNumber_t motorNumber, uint8_t kvalRunValue);

/*************************************************************************************************
 *  @brief              Gets the actual speed of the motor.                                     **
 *                                                                                              **
 *  @param motorNumber  The number of the motor in the motor array (check enum                  **
 *                      motorNumber_t in stepperMotorCommands.h).                               **
 *  @param speed        The pointer to the speed parameter.                                     **
 *                                                                                              **
 *  @return             True if procedure executed successfully, otherwise false.               **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorGetActualSpeed(motorNumber_t motorNumber, uint32_t *speed);

/*************************************************************************************************
 *  @brief                   Gets the absolute position of the motor.                           **
 *                                                                                              **
 *  @param motorNumber       The number of the motor in the motor array (check enum             **
 *                           motorNumber_t in stepperMotorCommands.h).                          **
 *  @param absolutePosition  The pointer to the position parameter.                             **
 *                                                                                              **
 *  @return                  True if procedure executed successfully, otherwise false.          **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorGetAbsPosition(motorNumber_t motorNumber, uint32_t *absolutePosition);

/*************************************************************************************************
 *  @brief                  Gets the actual microstepping mode of the motor                     **
 *                                                                                              **
 *  @param motorNumber      The number of the motor in the motor array (check enum              **
 *                          motorNumber_t in stepperMotorCommands.h).                           **
 *  @param stepResolution   The pointer to the step resolution parameter.                       **
 *                                                                                              **
 *  @return                 True if procedure executed successfully, otherwise false.           **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorGetMicrosteppingMode(motorNumber_t motorNumber, uint32_t *stepResolution);

/*************************************************************************************************
 *  @brief              Run motor constantly (through infinity and beyond).                     **
 *                                                                                              **
 *  @param motorNumber  The number of the motor in the motor array (check enum                  **
 *                      motorNumber_t in stepperMotorCommands.h).                               **
 *  @param direction    Move the motor in clockwise or counterclockwise direction.              **
 *  @param speed        The speed of the motor (range: 0x00 - 0x41).                            **
 *                                                                                              **
 *  @return             True if command executed successfully, otherwise false.                 **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorRun(motorNumber_t motorNumber, motorDirection_t direction, uint32_t speed);

/*************************************************************************************************
 *  @brief              Move motor with n-steps.                                                **
 *                                                                                              **
 *  @param motorNumber  The number of the motor in the motor array (check enum                  **
 *                      motorNumber_t in stepperMotorCommands.h).                               **
 *  @param direction    Move the motor in clockwise or counterclockwise direction.              **
 *  @param stepNumber   Number of given steps for the motor to take.                            **
 *                                                                                              **
 *  @return             True if command executed successfully, otherwise false.                 **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorMoveWithSteps(motorNumber_t motorNumber, motorDirection_t direction, uint32_t stepNumber);

/*************************************************************************************************
 *  @brief              Go until an endstop is triggered (SW pin is LOW).                       **
 *                                                                                              **
 *  @param motorNumber  The number of the motor in the motor array (check enum                  **
 *                      motorNumber_t in stepperMotorCommands.h).                               **
 *  @param action       The action that will be used while executing command (check enum        **
 *                      motorAction_t in stepperMotorDriverCommands.h)                          **
 *  @param direction    Move the motor in clockwise or counterclockwise direction.              **
 *  @param speed        The speed of the motor (range: 0x00 - 0x41).                            **
 *                                                                                              **
 *  @return             True if command executed successfully, otherwise false.                 **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorGoToHomePosition(motorNumber_t motorNumber, motorAction_t action, motorDirection_t direction, uint32_t speed);

/*************************************************************************************************
 *  @brief              Reset device to default parameters (all fields placed in the register   **
 *                      structure f.e. MOT_MAX_SPEED lose their values and are initizalized     **
 *                      with default ones described in the L6480H datasheet.                    **
 *                                                                                              **
 *  @param motorNumber  The number of the motor in the motor array (check enum motorNumber_t    **
 *                      in stepperMotorCommands.h).                                             **
 *                                                                                              **
 *  @return             True if device resetted to default parameters successfully,             **
 *                      otherwise false.                                                        **
 *                                                                                              **
 *************************************************************************************************/
bool StepperMotorResetDeviceToDefaultParameters(motorNumber_t motorNumber);



/****************************** END OF FILE  *********************************/
