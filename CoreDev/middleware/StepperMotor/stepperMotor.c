
/*****************************************************************************
 *
 * @file StepperMotor/StepperMotor.c
 *
 * @brief Source file for the low layer of the motor driver L6480H
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/

#include "middleware/StepperMotor/stepperMotor.h"

#include "driver/AssertDebug/debug.h"
#include "driver/McuDriver/mcuDriver.h"
#include "driver/StepperMotorDriver/stepperMotorCommands.h"
#include "driver/StepperMotorDriver/stepperMotorDriver.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#ifdef CFG_DEBUG_MODULE_NAME
#undef CFG_DEBUG_MODULE_NAME
#endif

#define CFG_DEBUG_MODULE_NAME "[STEP_MOTOR] "
#define THERMAL_SHUTDOWN_OFFSET (3U)
#define BYTE_SIZE (8U)

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
 *****************************************************************************/

typedef struct {
    uint32_t absolutePosition;
    uint16_t electricalPosition;
    uint32_t mark;
    uint32_t speed;
    uint16_t acceleration;
    uint16_t deceleration;
    uint16_t maxSpeed;
    uint16_t minSpeed;
    uint16_t fsSpeed;
    uint8_t kvalHold;
    uint8_t kvalRun;
    uint8_t kvalAcceleration;
    uint8_t kvalDeceleration;
    uint16_t intBemfSpeed;
    uint8_t stBemfSlope;
    uint8_t fnBemfSlopeAcceleration;
    uint8_t fnBemfSlopeDeceleration;
    uint8_t kTherm;
    uint8_t adcOut;
    uint8_t overcurrentThresholdValue;
    uint8_t stallThreshold;
    uint8_t microsteppingMode;
    uint8_t alarmEnable;
    uint16_t gateConfig1;
    uint8_t gateConfig2;
    uint16_t config;
    uint16_t status;
} motorParameterStructure_t;

static motorParameterStructure_t sMotorParameterStructure[NUMBER_OF_STEPPER_MOTORS] = { 0 };

/*****************************************************************************
                         PRIVATE FUNCTION DECLARATION
 *****************************************************************************/

/*************************************************************************************************
 *  @brief                  Set one parameter of a motor with a concrete value                  **
 *                                                                                              **
 *  @param motorNumber      The number of the motor in the motor array (check enum              **
 *                          motorNumber_t in stepperMotorCommands.h).                           **
 *  @param parameter        Chosen parameter to be changed (the list is given in                **
 *                          motorRegisters_t enum).                                             **
 *  @param parametervalue   The value of the parameter to be set.                               **
 *                                                                                              **
 *  @return                 True if parameter set successfully, otherwise false.                 **
 *                                                                                              **
 *************************************************************************************************/
static bool StepperMotorSetParameter(motorNumber_t motorNumber, motorRegisters_t parameter, uint32_t parametervalue);

/*************************************************************************************************
 *  @brief                  Get the value of a parameter stored in the internal memory of the   **
 *                          L6480H driver.                                                      **
 *                                                                                              **
 *  @param motorNumber      The number of the motor in the motor array (check enum              **
 *                          motorNumber_t in stepperMotorCommands.h).                           **
 *  @param parameter        Chosen parameter to be changed (the list is given in                **
 *                          motorRegisters_t enum).                                             **
 *  @param parametervalue   Pointer to receive data from driver.                                **
 *                                                                                              **
 *  @return                 True if parameter received successfully, otherwise false.           **
 *                                                                                              **
 *************************************************************************************************/
static bool StepperMotorGetParameter(motorNumber_t motorNumber, motorRegisters_t parameter, uint32_t *parameterValue);

/*************************************************************************************************
 *  @brief                  Get the value of the status register in the L6480H driver.          **
 *                                                                                              **
 *  @param motorNumber      The number of the motor in the motor array (check enum              **
 *                          motorNumber_t in stepperMotorCommands.h).                           **
 *  @param status           Pointer to the status data frame.                                   **
 *                                                                                              **
 *  @return                 True if status received successfully, otherwise false.              **
 *                                                                                              **
 *************************************************************************************************/
static bool StepperMotorGetStatus(motorNumber_t motorNumber, uint32_t *status);

/*************************************************************************************************
 *  @brief                  Print all errors that are active in the L6480H driver.              **
 *                                                                                              **
 *  @param parameterValue   Parameter frame containing all information about the status of the  **
 *                          motor, including potential errors                                   **
 *                                                                                              **
 *************************************************************************************************/
static void StepperMotorInterpretErrorStatusRegister(uint32_t parameterValue);

/*************************************************************************************************
 *  @brief                  Initialize register structures with values saved in                 **
 *                          stepperMotorConfig.h.                                               **
 *                                                                                              **
 *************************************************************************************************/
static void StepperMotorInitRegisterStructures(void);

/*************************************************************************************************
 *  @brief                  Set register structures in the L6480H driver.                       **
 *                                                                                              **
 *  @return                 True if parameters set successfully, otherwise false.                **
 *                                                                                              **
 *************************************************************************************************/
static bool StepperMotorSetRegisterStructures(void);

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
 *****************************************************************************/

bool StepperMotorInit(void)
{
    for (uint8_t i = 0; i < NUMBER_OF_STEPPER_MOTORS; i++) {

        /* Enable driver.
        ****************************************************************/
        MotorDriverSetDriverStatus(i, DRIVER_ENABLED);

        /* Set high impedance for the driver.
        ****************************************************************/
        if (!StepperMotorHardHiZ(i)) {
            LOG_ERROR("Failed to initialize stepper motor[%u]. High impedance not set correctly.", i);
            return false;
        }

        /* Start encoder timer.
        ****************************************************************/
        if (!MotorDriverStartEncoderTimer(i)) {
            LOG_ERROR("Failed to initialize encoder timer for the stepper motor[%u].", i);
            return false;
        }
    }

    /* Initialize register structures with parameters set in stepperMotorConfig.h.
    ******************************************************************************/
    StepperMotorInitRegisterStructures();

    /* Send the data stored register structures to the driver.
    ****************************************************************/
    if (!StepperMotorSetRegisterStructures()) {
        LOG_ERROR("Failed to initialize stepper motor register structures.");
        return false;
    }

    LOG_INFO("Successfully initialized stepper motors.");
    return true;
}

bool StepperMotorDeinit(void)
{
    for (uint8_t i = 0; i < NUMBER_OF_STEPPER_MOTORS; i++) {

        /* Disable driver.
        ****************************************************************/
        MotorDriverSetDriverStatus(i, DRIVER_DISABLED);

        /* Set high impedance for the driver.
        ****************************************************************/
        if (!StepperMotorHardHiZ(i)) {
            LOG_ERROR("Failed to initialize stepper motor[%u]. High impedance not set correctly.", i);
            return false;
        }

        /* Reset device to default parameters.
        ****************************************************************/
        if (!StepperMotorResetDeviceToDefaultParameters(i)) {
            LOG_ERROR("Failed to deinitialize stepper motor[%u]. Could not reset "
                      "motor to default parameters.",
                      i);
            return false;
        }
    }

    LOG_INFO("Successfully deinitialized stepper motors.");

    return true;
}

bool StepperMotorIsCommandDone(motorNumber_t motorNumber)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    /* Check the state of the busy pin.
    ****************************************************************/
    if (MotorDriverIsBusyPinSet(motorNumber)) {
        return false;
    }

    LOG_INFO("Command on motor[%u] done.", motorNumber);
    return true;
}

bool StepperMotorIsEndstopReached(motorNumber_t motorNumber)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    /* Check the state of the SW pin.
    ****************************************************************/
    if (MotorDriverIsEndstopPinSet(motorNumber)) {
        return false;
    }

    LOG_INFO("Home position on motor[%u] reached.", motorNumber);
    return true;
}

bool StepperMotorIsErrorDetected(motorNumber_t motorNumber)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    uint32_t parameterValue = 0;

    /* Check the state of the Error Flag pin.
    ****************************************************************/
    if (MotorDriverIsErrorPinSet(motorNumber)) {
        LOG_ERROR("Error detected! Checking the source of the error..");

        /* Get the status register.
        ****************************************************************/
        if (!StepperMotorGetStatus(motorNumber, &parameterValue)) {
            LOG_ERROR("Failed to receive source of error.");
            return false;
        }

        /* Process the received status register and log the source of the error.
        ************************************************************************/
        StepperMotorInterpretErrorStatusRegister(parameterValue);
        return true;
    }
    return false;
}

bool StepperMotorResetStatusRegister(motorNumber_t motorNumber)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    uint32_t parameterValue = 0;

    /* Reset the status register.
     * The command just needs to be executed - we don't need to know the content of the register.
     **********************************************************************************************/
    if (!StepperMotorGetStatus(motorNumber, &parameterValue)) {  //
        LOG_ERROR("Failed to reset status register of motor[%d].", motorNumber);
        return false;
    }
    return true;
}

bool StepperMotorPrintAllParameters(motorNumber_t motorNumber)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    uint32_t parameterValue = 0;
    bool status = true;
    const char line[] = "----------------------------";
    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_ABS_POS, &parameterValue);
    LOG_INFO("| ABS_POS     |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_EL_POS, &parameterValue);
    LOG_INFO("| EL_POS      |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_MARK, &parameterValue);
    LOG_INFO("| MARK        |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_SPEED, &parameterValue);
    LOG_INFO("| SPEED       |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_ACC, &parameterValue);
    LOG_INFO("| ACC         |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_DEC, &parameterValue);
    LOG_INFO("| DEC         |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_MAX_SPEED, &parameterValue);
    LOG_INFO("| MAX_SPEED   |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_MIN_SPEED, &parameterValue);
    LOG_INFO("| MIN_SPEED   |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_KVAL_HOLD, &parameterValue);
    LOG_INFO("| KVAL_HOLD   |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_KVAL_RUN, &parameterValue);
    LOG_INFO("| KVAL_RUN    |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_KVAL_ACC, &parameterValue);
    LOG_INFO("| KVAL_ACC    |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_KVAL_DEC, &parameterValue);
    LOG_INFO("| KVAL_DEC    |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_INT_SPD, &parameterValue);
    LOG_INFO("| INT_SPD     |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_ST_SLP, &parameterValue);
    LOG_INFO("| ST_SLP      |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_FN_SLP_ACC, &parameterValue);
    LOG_INFO("| FN_SLP_ACC  |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_FN_SLP_DEC, &parameterValue);
    LOG_INFO("| FN_SLP_DEC  |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_K_THERM, &parameterValue);
    LOG_INFO("| K_THERM     |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_OCD_TH, &parameterValue);
    LOG_INFO("| OCD_TH      |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_STALL_TH, &parameterValue);
    LOG_INFO("| STALL_TH    |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_FS_SPD, &parameterValue);
    LOG_INFO("| FS_SPD      |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_STEP_MODE, &parameterValue);
    LOG_INFO("| STEP_MODE   |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_ALARM_EN, &parameterValue);
    LOG_INFO("| ALARM_EN    |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_CONFIG, &parameterValue);
    LOG_INFO("| CONFIG      |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetParameter(motorNumber, MOT_STATUS, &parameterValue);
    LOG_INFO("| STATUS NR 1 |     %d     |", parameterValue);

    LOG_INFO("%s", line);
    status &= StepperMotorGetStatus(motorNumber, &parameterValue);
    LOG_INFO("| STATUS NR 2 |     %d     |", parameterValue);

    if (!status) {
        LOG_ERROR("Failed to obtain at least one parameter.", motorNumber);
        return false;
    }
    return true;
}

bool StepperMotorSoftStop(motorNumber_t motorNumber)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    /* Send soft stop command.
    ****************************************************************/
    if (!MotorDriverWriteByte(motorNumber, MOT_SOFT_STOP)) {
        LOG_ERROR("Failed to soft stop motor[%d].", motorNumber);
        return false;
    }

    LOG_INFO("Motor[%d] has soft stopped.", motorNumber);

    return true;
}

bool StepperMotorHardStop(motorNumber_t motorNumber)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    /* Send hard stop command.
    ****************************************************************/
    if (!MotorDriverWriteByte(motorNumber, MOT_HARD_STOP)) {
        LOG_ERROR("Failed to hard stop motor[%d].", motorNumber);
        return false;
    }

    LOG_INFO("Motor[%d] has hard stopped.", motorNumber);

    return true;
}

bool StepperMotorSoftHiZ(motorNumber_t motorNumber)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    /* Send soft high impedance command.
    ****************************************************************/
    if (!MotorDriverWriteByte(motorNumber, MOT_SOFT_HIZ)) {
        LOG_ERROR("Failed to set high impedance in soft mode on motor[%d].", motorNumber);
        return false;
    }

    LOG_INFO("Motor[%d] has entered soft high impedance.", motorNumber);

    return true;
}

bool StepperMotorHardHiZ(motorNumber_t motorNumber)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    /* Send hard high impedance command.
    ****************************************************************/
    if (!MotorDriverWriteByte(motorNumber, MOT_HARD_HIZ)) {
        LOG_ERROR("Failed to set high impedance in hard mode on motor[%d].", motorNumber);
        return false;
    }

    LOG_INFO("Motor[%d] has entered hard high impedance.", motorNumber);

    return true;
}

bool StepperMotorReleaseSW(motorNumber_t motorNumber, motorAction_t action, motorDirection_t direction)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    /* Send release SW command, which is used to release the switch input of the driver
    ***********************************************************************************/
    if (!MotorDriverWriteByte(motorNumber, (uint8_t)MOT_RELEASE_SW | (uint8_t)action | (uint8_t)direction)) {
        LOG_ERROR("Failed to release SW mode on motor[%d].", motorNumber);
        return false;
    }

    LOG_INFO("Released SW mode on motor[%d].", motorNumber);

    return true;
}

bool StepperMotorSetMicrosteppingMode(motorNumber_t motorNumber, motorStepSel_t stepResolution)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    bool status = true;

    /* In order to set microstepping mode, the motor needs to be stopped and in high impedance state.
    *************************************************************************************************/
    status &= StepperMotorSoftStop(motorNumber);
    status &= StepperMotorSoftHiZ(motorNumber);

    /* Set the microstepping parameter with the enumerated resolution.
    ******************************************************************/
    status &= StepperMotorSetParameter(motorNumber, MOT_STEP_MODE, (uint32_t)stepResolution);

    if (!status) {
        LOG_ERROR("Failed to set microstepping mode on motor[%d].", motorNumber);
        return false;
    }
    LOG_INFO("Stepper motor[%d] set to microstepping mode[%d]", motorNumber, stepResolution);
    return true;
}

bool StepperMotorSetAccelerationDecelaration(motorNumber_t motorNumber, uint16_t acceleration, uint16_t deceleration)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    bool status = true;

    /* Convert the raw acceleration and deceleration values to a normalized parameter format.
    *****************************************************************************************/
    uint16_t accelerationParameter = (CONV_ACC_DEC_STEPS_TO_PARAM(acceleration));
    uint16_t decelerationParameter = (CONV_ACC_DEC_STEPS_TO_PARAM(deceleration));

    /* In order to set ACC and DEC, the motor needs to be stopped.
    **************************************************************/
    status &= StepperMotorSoftStop(motorNumber);

    /* Set the ACC and DEC parameters with the converted data.
    **********************************************************/
    status &= StepperMotorSetParameter(motorNumber, MOT_ACC, accelerationParameter);
    status &= StepperMotorSetParameter(motorNumber, MOT_DEC, decelerationParameter);

    if (!status) {
        LOG_ERROR("Failed to set acceleration and deceleration on motor[%d].", motorNumber);
        return false;
    }
    LOG_INFO("Stepper motor[%d] set to acceleration[%d] and deceleration[%d]", motorNumber, acceleration, deceleration);
    return true;
}

bool StepperMotorSetMaximumSpeed(motorNumber_t motorNumber, uint32_t speed)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    bool status = true;

    /* Convert the raw speed value to a normalized parameter format.
    ****************************************************************/
    uint16_t speedParameter = CONV_SPEED_DEFAULT_STEPS_TO_PARAM(speed);

    /* Set the MAX_SPEED parameter with the converted data.
    **********************************************************/
    status &= StepperMotorSetParameter(motorNumber, MOT_MAX_SPEED, speedParameter);

    if (!status) {
        LOG_ERROR("Failed to set default speed on motor[%d].", motorNumber);
        return false;
    }
    LOG_INFO("Stepper motor[%d] set to default speed[%d].", motorNumber, speed);
    return true;
}

bool StepperMotorSetAbsolutePosition(motorNumber_t motorNumber, uint32_t absolutePosition)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    bool status = true;

    /* In order to set absolute position, the motor needs to be stopped and in high impedance state.
    *************************************************************************************************/
    status &= StepperMotorSoftStop(motorNumber);
    status &= StepperMotorSoftHiZ(motorNumber);

    /* Set the MOT_ABS_POS parameter with the converted data.
    **********************************************************/
    status &= StepperMotorSetParameter(motorNumber, MOT_ABS_POS, absolutePosition);

    if (!status) {
        LOG_ERROR("Failed to set absolute position on motor[%d].", motorNumber);
        return false;
    }
    LOG_INFO("Stepper motor[%d] set to absolute position[%d].", motorNumber, absolutePosition);
    return true;
}

bool StepperMotorSetOvercurrentThreshold(motorNumber_t motorNumber, uint8_t maximumThreshold)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    bool status = true;

    /* Set the MOT_OCD_TH parameter with the given data.
    **********************************************************/
    status &= StepperMotorSetParameter(motorNumber, MOT_OCD_TH, maximumThreshold);

    if (!status) {
        LOG_ERROR("Failed to set maximum overcurrent threshold on motor[%d].", motorNumber);
        return false;
    }
    LOG_INFO("Stepper motor[%d] set with maximum overcurrent threshold[%d].", motorNumber, maximumThreshold);
    return true;
}

bool StepperMotorSetStallThreshold(motorNumber_t motorNumber, uint8_t maximumThreshold)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    bool status = true;

    /* Set the MOT_STALL_TH parameter with the given data.
    **********************************************************/
    status &= StepperMotorSetParameter(motorNumber, MOT_STALL_TH, maximumThreshold);

    if (!status) {
        LOG_ERROR("Failed to set maximum stall threshold on motor[%d].", motorNumber);
        return false;
    }
    LOG_INFO("Stepper motor[%d] set with maximum stall threshold[%d].", motorNumber, maximumThreshold);
    return true;
}

bool StepperMotorSetKTherm(motorNumber_t motorNumber, uint8_t kthermValue)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    bool status = true;

    /* Set the MOT_K_THERM parameter with the given data.
    **********************************************************/
    status &= StepperMotorSetParameter(motorNumber, MOT_K_THERM, kthermValue);

    if (!status) {
        LOG_ERROR("Failed to set ktherm value on motor[%d].", motorNumber);
        return false;
    }
    LOG_INFO("Stepper motor[%d] set with ktherm value[%d].", motorNumber, kthermValue);
    return true;
}

bool StepperMotorSetKvalRun(motorNumber_t motorNumber, uint8_t kvalRunValue)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    bool status = true;

    /* Set the MOT_KVAL_RUN parameter with the given data.
    **********************************************************/
    status &= StepperMotorSetParameter(motorNumber, MOT_KVAL_RUN, kvalRunValue);

    if (!status) {
        LOG_ERROR("Failed to set KvalRun value on motor[%d].", motorNumber);
        return false;
    }
    LOG_INFO("Stepper motor[%d] set with KvalRun value[%d].", motorNumber, kvalRunValue);
    return true;
}

bool StepperMotorGetActualSpeed(motorNumber_t motorNumber, uint32_t *speed)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));
    ASSERT(speed);

    /* Receive the MOT_SPEED parameter.
    **********************************************************/
    return StepperMotorGetParameter(motorNumber, MOT_SPEED, speed);
}

bool StepperMotorGetAbsPosition(motorNumber_t motorNumber, uint32_t *absolutePosition)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));
    ASSERT(absolutePosition);

    /* Receive the MOT_ABS_POS parameter.
    **********************************************************/
    return StepperMotorGetParameter(motorNumber, MOT_ABS_POS, absolutePosition);
}

bool StepperMotorGetMicrosteppingMode(motorNumber_t motorNumber, uint32_t *stepResolution)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));
    ASSERT(stepResolution);

    /* Receive the MOT_STEP_MODE parameter.
    **********************************************************/
    return StepperMotorGetParameter(motorNumber, MOT_STEP_MODE, (uint32_t *)stepResolution);
}

bool StepperMotorRun(motorNumber_t motorNumber, motorDirection_t direction, uint32_t speed)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    bool status = true;

    /* Send the MOT_RUN command with the speed and direction parameters.
    ********************************************************************/
    status &= MotorDriverWriteByte(motorNumber, (uint8_t)MOT_RUN | (uint8_t)direction);
    status &= MotorDriverWriteByte(motorNumber, (uint8_t)(speed >> 16));
    status &= MotorDriverWriteByte(motorNumber, (uint8_t)(speed >> 8));
    status &= MotorDriverWriteByte(motorNumber, (uint8_t)(speed));

    return status;
}

bool StepperMotorMoveWithSteps(motorNumber_t motorNumber, motorDirection_t direction, uint32_t stepNumber)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    bool status = true;

    /* Send the MOT_MOVE command with the number of microsteps to be moved and direction parameter.
    ************************************************************************************************/
    status &= MotorDriverWriteByte(motorNumber, (uint8_t)MOT_MOVE | (uint8_t)direction);
    status &= MotorDriverWriteByte(motorNumber, (uint8_t)(stepNumber >> 16));
    status &= MotorDriverWriteByte(motorNumber, (uint8_t)(stepNumber >> 8));
    status &= MotorDriverWriteByte(motorNumber, (uint8_t)(stepNumber));

    return status;
}

bool StepperMotorGoToHomePosition(motorNumber_t motorNumber, motorAction_t action, motorDirection_t direction, uint32_t speed)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    bool status = true;

    /* Send the MOT_GO_UNTIL command with the speed, action and direction parameters.
    *********************************************************************************/
    status &= MotorDriverWriteByte(motorNumber, (uint8_t)MOT_GO_UNTIL | (uint8_t)action | (uint8_t)direction);
    status &= MotorDriverWriteByte(motorNumber, (uint8_t)(speed >> 16));
    status &= MotorDriverWriteByte(motorNumber, (uint8_t)(speed >> 8));
    status &= MotorDriverWriteByte(motorNumber, (uint8_t)(speed));

    return status;
}

bool StepperMotorResetDeviceToDefaultParameters(motorNumber_t motorNumber)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    /* Send the MOT_RESET_DEVICE to reset the parameters stored in the driver.
    **************************************************************************/
    return MotorDriverWriteByte(motorNumber, MOT_RESET_DEVICE);
}

/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
 ******************************************************************************/

static bool StepperMotorSetParameter(motorNumber_t motorNumber, motorRegisters_t parameter, uint32_t parameterValue)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    uint32_t parameterValueRead = 0;

    bool status = true;

    /* Send the MOT_SET_PARAM command and with the appropriate parameter to be set.
    *******************************************************************************/
    status &= MotorDriverWriteByte(motorNumber, MOT_SET_PARAM | parameter);

    /* Switch with several fallthroughs used to send the required number of bytes.
    *******************************************************************************/
    switch (parameter) {
    case MOT_ABS_POS:
    case MOT_MARK:
        status &= MotorDriverWriteByte(motorNumber, (uint8_t)(parameterValue >> 16));
        status &= MotorDriverWriteByte(motorNumber, (uint8_t)(parameterValue >> 8));
        status &= MotorDriverWriteByte(motorNumber, (uint8_t)(parameterValue));
        break;

    case MOT_EL_POS:
    case MOT_ACC:
    case MOT_DEC:
    case MOT_MAX_SPEED:
    case MOT_MIN_SPEED:
    case MOT_FS_SPD:
    case MOT_INT_SPD:
    case MOT_CONFIG:
    case MOT_STATUS:
    case MOT_GATECFG1:
        status &= MotorDriverWriteByte(motorNumber, (uint8_t)(parameterValue >> 8));
        status &= MotorDriverWriteByte(motorNumber, (uint8_t)(parameterValue));
        break;

    case MOT_KVAL_HOLD:
    case MOT_KVAL_RUN:
    case MOT_KVAL_ACC:
    case MOT_KVAL_DEC:
    case MOT_ST_SLP:
    case MOT_FN_SLP_ACC:
    case MOT_FN_SLP_DEC:
    case MOT_K_THERM:
    case MOT_OCD_TH:
    case MOT_STALL_TH:
    case MOT_STEP_MODE:
    case MOT_ALARM_EN:
    case MOT_GATECFG2:
        status &= MotorDriverWriteByte(motorNumber, (uint8_t)(parameterValue));
        break;

    default:
        break;
    }

    /* Check if the parameter was successfully set.
    *******************************************************************************/
    status &= StepperMotorGetParameter(motorNumber, parameter, &parameterValueRead);
    if (parameterValue != parameterValueRead) {
        status = false;
    }
    return status;
}

static bool StepperMotorGetParameter(motorNumber_t motorNumber, motorRegisters_t parameter, uint32_t *parameterValue)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));

    uint8_t numberOfBytes = 0;

    /* Determine how many bytes need to be received.
    ****************************************************/
    switch (parameter) {
    case MOT_ABS_POS:
    case MOT_MARK:
    case MOT_SPEED:
        numberOfBytes = 3;
        break;

    case MOT_EL_POS:
    case MOT_ACC:
    case MOT_DEC:
    case MOT_MAX_SPEED:
    case MOT_MIN_SPEED:
    case MOT_FS_SPD:
    case MOT_INT_SPD:
    case MOT_CONFIG:
    case MOT_STATUS:
        numberOfBytes = 2;
        break;

    case MOT_KVAL_HOLD:
    case MOT_KVAL_RUN:
    case MOT_KVAL_ACC:
    case MOT_KVAL_DEC:
    case MOT_ST_SLP:
    case MOT_FN_SLP_ACC:
    case MOT_FN_SLP_DEC:
    case MOT_K_THERM:
    case MOT_OCD_TH:
    case MOT_STALL_TH:
    case MOT_STEP_MODE:
    case MOT_ALARM_EN:
        numberOfBytes = 1;
        break;

    default:
        numberOfBytes = 0;
        break;
    }

    if (numberOfBytes == 0) {
        return false;
    }

    /* Receive the register content.
    ****************************************************/
    return MotorDriverReadBytes(motorNumber, MOT_GET_PARAM | parameter, parameterValue, numberOfBytes);
}

static bool StepperMotorGetStatus(motorNumber_t motorNumber, uint32_t *status)
{
    ASSERT((motorNumber == STEPPER_MOTOR_X) || (motorNumber == STEPPER_MOTOR_Y) || (motorNumber == STEPPER_MOTOR_Z));
    ASSERT(status);

    /* Receive the MOT_GET_STATUS register content.
    ****************************************************/
    return MotorDriverReadBytes(motorNumber, MOT_GET_STATUS, status, 2);
}

static void StepperMotorInterpretErrorStatusRegister(uint32_t parameterValue)
{
    uint16_t parameterValueU16 = (uint16_t)parameterValue;

    /* Interpret single bits in the status register to determine which
     * fault cause the error flag to be raised.
     *********************************************************************/
    if ((parameterValueU16 & 0x0008) != 0) {
        LOG_ERROR("Detected switch turn on event!");
    }
    if ((parameterValueU16 & 0x0080) != 0) {
        LOG_ERROR("Detected command error!");
    }
    if ((parameterValueU16 & 0x0200) == 0) {
        LOG_ERROR("Detected undervoltage!");
    }
    if ((parameterValueU16 & 0x0400) == 0) {
        LOG_ERROR("Detected undervoltage of the ADC!");
    }
    if (((parameterValueU16 & 0x0800) != 0) || ((parameterValueU16 & 0x1000) != 0)) {
        uint8_t thermalShutdown = (parameterValueU16 & 0x1800) >> (BYTE_SIZE + THERMAL_SHUTDOWN_OFFSET);
        switch (thermalShutdown) {
        case MOT_THERM_WARNING:
            LOG_ERROR("Thermal warning flag raised.");
            break;
        case MOT_THERM_BRIDGE_SHUTDOWN:
            LOG_ERROR("Thermal bridge shutdown.");
            break;
        case MOT_THERM_DEVICE_SHUTDOWN:
            LOG_ERROR("Thermal device shutdown.");
            break;
        default:
            LOG_INFO("Thermal in appropiate range.");
            break;
        }
    }
    if ((parameterValueU16 & 0x2000) == 0) {
        LOG_ERROR("Detected overcurrent!");
    }
    if ((parameterValueU16 & 0x4000) == 0) {
        LOG_ERROR("Detected stall on bridge A");
    }
    if ((parameterValueU16 & 0x8000) == 0) {
        LOG_ERROR("Detected stall on bridge B");
    }
}

static void StepperMotorInitRegisterStructures(void)
{
    /* Given values come from the datasheet of the L6480H;
     * for more information check the Register and Flag description (chapter 9).
     ***************************************************************************/
    sMotorParameterStructure[STEPPER_MOTOR_X].absolutePosition = REG_MOTOR_X_ABS_POS;
    sMotorParameterStructure[STEPPER_MOTOR_X].electricalPosition = REG_MOTOR_X_EL_POS;
    sMotorParameterStructure[STEPPER_MOTOR_X].mark = REG_MOTOR_X_MARK;
    sMotorParameterStructure[STEPPER_MOTOR_X].acceleration = REG_MOTOR_X_ACCELERATION;
    sMotorParameterStructure[STEPPER_MOTOR_X].deceleration = REG_MOTOR_X_DECELERATION;
    sMotorParameterStructure[STEPPER_MOTOR_X].maxSpeed = REG_MOTOR_X_MAXIMUM_SPEED;
    sMotorParameterStructure[STEPPER_MOTOR_X].minSpeed = REG_MOTOR_X_MINIMAL_SPEED;
    sMotorParameterStructure[STEPPER_MOTOR_X].fsSpeed = REG_MOTOR_X_FS_SPD;
    sMotorParameterStructure[STEPPER_MOTOR_X].kvalHold = REG_MOTOR_X_KVAL_HOLD;
    sMotorParameterStructure[STEPPER_MOTOR_X].kvalRun = REG_MOTOR_X_KVAL_RUN;
    sMotorParameterStructure[STEPPER_MOTOR_X].kvalAcceleration = REG_MOTOR_X_KVAL_ACC;
    sMotorParameterStructure[STEPPER_MOTOR_X].kvalDeceleration = REG_MOTOR_X_KVAL_DEC;
    sMotorParameterStructure[STEPPER_MOTOR_X].intBemfSpeed = REG_MOTOR_X_INT_SPD;
    sMotorParameterStructure[STEPPER_MOTOR_X].stBemfSlope = REG_MOTOR_X_ST_SLP;
    sMotorParameterStructure[STEPPER_MOTOR_X].fnBemfSlopeAcceleration = REG_MOTOR_X_FN_SLP_ACC;
    sMotorParameterStructure[STEPPER_MOTOR_X].fnBemfSlopeDeceleration = REG_MOTOR_X_FN_SLP_DEC;
    sMotorParameterStructure[STEPPER_MOTOR_X].kTherm = REG_MOTOR_X_K_THERM;
    sMotorParameterStructure[STEPPER_MOTOR_X].overcurrentThresholdValue = REG_MOTOR_X_OCD_TH;
    sMotorParameterStructure[STEPPER_MOTOR_X].stallThreshold = REG_MOTOR_X_STALL_TH;
    sMotorParameterStructure[STEPPER_MOTOR_X].microsteppingMode = REG_MOTOR_X_STEP_MODE;
    sMotorParameterStructure[STEPPER_MOTOR_X].alarmEnable = REG_MOTOR_X_ALARM_EN;
    sMotorParameterStructure[STEPPER_MOTOR_X].gateConfig1 = REG_MOTOR_X_GATECFG1;
    sMotorParameterStructure[STEPPER_MOTOR_X].gateConfig2 = REG_MOTOR_X_GATECFG2;
    sMotorParameterStructure[STEPPER_MOTOR_X].config = REG_MOTOR_X_CONFIG;

    sMotorParameterStructure[STEPPER_MOTOR_Y].absolutePosition = REG_MOTOR_Y_ABS_POS;
    sMotorParameterStructure[STEPPER_MOTOR_Y].electricalPosition = REG_MOTOR_Y_EL_POS;
    sMotorParameterStructure[STEPPER_MOTOR_Y].mark = REG_MOTOR_Y_MARK;
    sMotorParameterStructure[STEPPER_MOTOR_Y].acceleration = REG_MOTOR_Y_ACCELERATION;
    sMotorParameterStructure[STEPPER_MOTOR_Y].deceleration = REG_MOTOR_Y_DECELERATION;
    sMotorParameterStructure[STEPPER_MOTOR_Y].maxSpeed = REG_MOTOR_Y_MAXIMUM_SPEED;
    sMotorParameterStructure[STEPPER_MOTOR_Y].minSpeed = REG_MOTOR_Y_MINIMAL_SPEED;
    sMotorParameterStructure[STEPPER_MOTOR_Y].fsSpeed = REG_MOTOR_Y_FS_SPD;
    sMotorParameterStructure[STEPPER_MOTOR_Y].kvalHold = REG_MOTOR_Y_KVAL_HOLD;
    sMotorParameterStructure[STEPPER_MOTOR_Y].kvalRun = REG_MOTOR_Y_KVAL_RUN;
    sMotorParameterStructure[STEPPER_MOTOR_Y].kvalAcceleration = REG_MOTOR_Y_KVAL_ACC;
    sMotorParameterStructure[STEPPER_MOTOR_Y].kvalDeceleration = REG_MOTOR_Y_KVAL_DEC;
    sMotorParameterStructure[STEPPER_MOTOR_Y].intBemfSpeed = REG_MOTOR_Y_INT_SPD;
    sMotorParameterStructure[STEPPER_MOTOR_Y].stBemfSlope = REG_MOTOR_Y_ST_SLP;
    sMotorParameterStructure[STEPPER_MOTOR_Y].fnBemfSlopeAcceleration = REG_MOTOR_Y_FN_SLP_ACC;
    sMotorParameterStructure[STEPPER_MOTOR_Y].fnBemfSlopeDeceleration = REG_MOTOR_Y_FN_SLP_DEC;
    sMotorParameterStructure[STEPPER_MOTOR_Y].kTherm = REG_MOTOR_Y_K_THERM;
    sMotorParameterStructure[STEPPER_MOTOR_Y].overcurrentThresholdValue = REG_MOTOR_Y_OCD_TH;
    sMotorParameterStructure[STEPPER_MOTOR_Y].stallThreshold = REG_MOTOR_Y_STALL_TH;
    sMotorParameterStructure[STEPPER_MOTOR_Y].microsteppingMode = REG_MOTOR_Y_STEP_MODE;
    sMotorParameterStructure[STEPPER_MOTOR_Y].alarmEnable = REG_MOTOR_Y_ALARM_EN;
    sMotorParameterStructure[STEPPER_MOTOR_Y].gateConfig1 = REG_MOTOR_Y_GATECFG1;
    sMotorParameterStructure[STEPPER_MOTOR_Y].gateConfig2 = REG_MOTOR_Y_GATECFG2;
    sMotorParameterStructure[STEPPER_MOTOR_Y].config = REG_MOTOR_Y_CONFIG;

    sMotorParameterStructure[STEPPER_MOTOR_Z].absolutePosition = REG_MOTOR_Z_ABS_POS;
    sMotorParameterStructure[STEPPER_MOTOR_Z].electricalPosition = REG_MOTOR_Z_EL_POS;
    sMotorParameterStructure[STEPPER_MOTOR_Z].mark = REG_MOTOR_Z_MARK;
    sMotorParameterStructure[STEPPER_MOTOR_Z].acceleration = REG_MOTOR_Z_ACCELERATION;
    sMotorParameterStructure[STEPPER_MOTOR_Z].deceleration = REG_MOTOR_Z_DECELERATION;
    sMotorParameterStructure[STEPPER_MOTOR_Z].maxSpeed = REG_MOTOR_Z_MAXIMUM_SPEED;
    sMotorParameterStructure[STEPPER_MOTOR_Z].minSpeed = REG_MOTOR_Z_MINIMAL_SPEED;
    sMotorParameterStructure[STEPPER_MOTOR_Z].fsSpeed = REG_MOTOR_Z_FS_SPD;
    sMotorParameterStructure[STEPPER_MOTOR_Z].kvalHold = REG_MOTOR_Z_KVAL_HOLD;
    sMotorParameterStructure[STEPPER_MOTOR_Z].kvalRun = REG_MOTOR_Z_KVAL_RUN;
    sMotorParameterStructure[STEPPER_MOTOR_Z].kvalAcceleration = REG_MOTOR_Z_KVAL_ACC;
    sMotorParameterStructure[STEPPER_MOTOR_Z].kvalDeceleration = REG_MOTOR_Z_KVAL_DEC;
    sMotorParameterStructure[STEPPER_MOTOR_Z].intBemfSpeed = REG_MOTOR_Z_INT_SPD;
    sMotorParameterStructure[STEPPER_MOTOR_Z].stBemfSlope = REG_MOTOR_Z_ST_SLP;
    sMotorParameterStructure[STEPPER_MOTOR_Z].fnBemfSlopeAcceleration = REG_MOTOR_Z_FN_SLP_ACC;
    sMotorParameterStructure[STEPPER_MOTOR_Z].fnBemfSlopeDeceleration = REG_MOTOR_Z_FN_SLP_DEC;
    sMotorParameterStructure[STEPPER_MOTOR_Z].kTherm = REG_MOTOR_Z_K_THERM;
    sMotorParameterStructure[STEPPER_MOTOR_Z].overcurrentThresholdValue = REG_MOTOR_Z_OCD_TH;
    sMotorParameterStructure[STEPPER_MOTOR_Z].stallThreshold = REG_MOTOR_Z_STALL_TH;
    sMotorParameterStructure[STEPPER_MOTOR_Z].microsteppingMode = REG_MOTOR_Z_STEP_MODE;
    sMotorParameterStructure[STEPPER_MOTOR_Z].alarmEnable = REG_MOTOR_Z_ALARM_EN;
    sMotorParameterStructure[STEPPER_MOTOR_Z].gateConfig1 = REG_MOTOR_Z_GATECFG1;
    sMotorParameterStructure[STEPPER_MOTOR_Z].gateConfig2 = REG_MOTOR_Z_GATECFG2;
    sMotorParameterStructure[STEPPER_MOTOR_Z].config = REG_MOTOR_Z_CONFIG;
}

static bool StepperMotorSetRegisterStructures(void)
{
    bool status = true;
    for (uint8_t motorNumber = 0; motorNumber < NUMBER_OF_STEPPER_MOTORS; motorNumber++) {
        status &= StepperMotorSetParameter(motorNumber, MOT_ABS_POS, sMotorParameterStructure[motorNumber].absolutePosition);
        status &= StepperMotorSetParameter(motorNumber, MOT_EL_POS, sMotorParameterStructure[motorNumber].electricalPosition);
        status &= StepperMotorSetParameter(motorNumber, MOT_MARK, sMotorParameterStructure[motorNumber].mark);
        status &= StepperMotorSetParameter(motorNumber, MOT_ACC, sMotorParameterStructure[motorNumber].acceleration);
        status &= StepperMotorSetParameter(motorNumber, MOT_DEC, sMotorParameterStructure[motorNumber].deceleration);
        status &= StepperMotorSetParameter(motorNumber, MOT_MAX_SPEED, sMotorParameterStructure[motorNumber].maxSpeed);
        status &= StepperMotorSetParameter(motorNumber, MOT_MIN_SPEED, sMotorParameterStructure[motorNumber].minSpeed);
        status &= StepperMotorSetParameter(motorNumber, MOT_FS_SPD, sMotorParameterStructure[motorNumber].fsSpeed);
        status &= StepperMotorSetParameter(motorNumber, MOT_KVAL_HOLD, sMotorParameterStructure[motorNumber].kvalHold);
        status &= StepperMotorSetParameter(motorNumber, MOT_KVAL_RUN, sMotorParameterStructure[motorNumber].kvalRun);
        status &= StepperMotorSetParameter(motorNumber, MOT_KVAL_ACC, sMotorParameterStructure[motorNumber].kvalAcceleration);
        status &= StepperMotorSetParameter(motorNumber, MOT_KVAL_DEC, sMotorParameterStructure[motorNumber].kvalDeceleration);
        status &= StepperMotorSetParameter(motorNumber, MOT_INT_SPD, sMotorParameterStructure[motorNumber].intBemfSpeed);
        status &= StepperMotorSetParameter(motorNumber, MOT_ST_SLP, sMotorParameterStructure[motorNumber].stBemfSlope);
        status &= StepperMotorSetParameter(motorNumber, MOT_FN_SLP_ACC, sMotorParameterStructure[motorNumber].fnBemfSlopeAcceleration);
        status &= StepperMotorSetParameter(motorNumber, MOT_FN_SLP_DEC, sMotorParameterStructure[motorNumber].fnBemfSlopeDeceleration);
        status &= StepperMotorSetParameter(motorNumber, MOT_K_THERM, sMotorParameterStructure[motorNumber].kTherm);
        status &= StepperMotorSetParameter(motorNumber, MOT_OCD_TH, sMotorParameterStructure[motorNumber].overcurrentThresholdValue);
        status &= StepperMotorSetParameter(motorNumber, MOT_STALL_TH, sMotorParameterStructure[motorNumber].stallThreshold);
        status &= StepperMotorSetParameter(motorNumber, MOT_STEP_MODE, sMotorParameterStructure[motorNumber].microsteppingMode);
        status &= StepperMotorSetParameter(motorNumber, MOT_ALARM_EN, sMotorParameterStructure[motorNumber].alarmEnable);
        status &= StepperMotorSetParameter(motorNumber, MOT_GATECFG1, sMotorParameterStructure[motorNumber].gateConfig1);
        status &= StepperMotorSetParameter(motorNumber, MOT_GATECFG2, sMotorParameterStructure[motorNumber].gateConfig2);
        status &= StepperMotorSetParameter(motorNumber, MOT_CONFIG, sMotorParameterStructure[motorNumber].config);
    }

    return status;
}

/****************************** END OF FILE  *********************************/
