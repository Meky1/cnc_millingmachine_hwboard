/*****************************************************************************
 * @file middleware/circBuff/circBuff.h
 *
 * @brief Generic circular buffer
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/

#pragma once

#include "CncConfig.h"

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
*****************************************************************************/

typedef struct {
    uint8_t *data;
    uint32_t read;
    uint32_t write;
    uint32_t size;
    bool full;
    bool isAllocated;
} circBuff_t;

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
*****************************************************************************/

/*************************************************************************************************
 *  @brief          Initializes the circular buffer and allocates memory with given size.       **
 *                                                                                              **
 *  @param cbuf     The pointer to the circular buffer.                                         **
 *  @param size     The data buffer size.                                                       **
 *                                                                                              **
 *  @return         True if memory allocation succeeded, otherwise false.                       **
 *                                                                                              **
 *************************************************************************************************/
bool CircBuffDynamicBufferInit(circBuff_t *cbuf, uint32_t size);

/*************************************************************************************************
 *  @brief          Initializes the circular buffer with pre-allocated data buffer.             **
 *                                                                                              **
 *  @param cbuf     The pointer to the circular buffer.                                         **
 *  @param dataBuf  The pre-allocated data buffer.                                              **
 *  @param size     The data buffer size.                                                       **
 *                                                                                              **
 *  @return         True if initalized successfully, otherwise false.                           **
 *                                                                                              **
 *************************************************************************************************/
bool CircBuffStaticBufferInit(circBuff_t *cbuf, uint8_t *dataBuf, uint32_t size);

/*************************************************************************************************
 *  @brief          Deinitializes the buffer. If memory was allocated, it is freed.             **
 *                                                                                              **
 *  @param cbuf     The pointer to the circular buffer.                                         **
 *                                                                                              **
 *************************************************************************************************/
void CircBuffDeinit(circBuff_t *cbuf);

/*************************************************************************************************
 *  @brief          Write "len" bytes to buffer (but no more than buffer size). If buffer       **
 *                  space is smaller than "len", oldest data is overwritten.                    **
 *                                                                                              **
 *  @param cbuf     The pointer to the circular buffer.                                         **
 *  @param data     Pointer to the data to write.                                               **
 *  @param length   Data count to write, but no more that buffer size.                          **
 *                                                                                              **
 *************************************************************************************************/
void CircBuffWrite(circBuff_t *cbuf, uint8_t *data, uint32_t length);

/*************************************************************************************************
 *  @brief              Read "lenToRead" data from buffer. Maximum length to read cannot        **
 *                      exceed buffer size. If less data is available, less data will be read.  **
 *                                                                                              **
 *  @param cbuf         The pointer to the circular buffer.                                     **
 *  @param dataOut      Pointer to the data output buffer, should be as big as least            **
 *                      "lenToRead".                                                            **
 *  @param lenToRead    The amount of data to read.                                             **
 *                                                                                              **
 *  @return             The amount of data actually read.                                       **
 *                                                                                              **
 *************************************************************************************************/
uint32_t CircBuffRead(circBuff_t *cbuf, uint8_t *dataOut, uint32_t lenToRead);

/*************************************************************************************************
 *  @brief              Copy "lenToRead" data into "dataOut" buffer without removing them       **
 *                      from the buffer.                                                        **
 *                                                                                              **
 *  @param cbuf         The pointer to the circular buffer.                                     **
 *  @param dataOut      Pointer to the data output buffer, should be as big as least            **
 *                      "lenToRead".                                                            **
 *  @param lenToRead    The amount of data to read.                                             **
 *                                                                                              **
 *  @return             The amount of data actually copied.                                     **
 *                                                                                              **
 *************************************************************************************************/
uint32_t CircBuffPeek(circBuff_t *cbuf, uint8_t *dataOut, uint32_t lenToRead);

/*************************************************************************************************
 *  @brief              Drops the data from buffer (marks given amount of data as read).        **
 *                                                                                              **
 *  @param cbuf         The pointer to the circular buffer.                                     **
 *  @param lenToDrop    The amount of data to drop.                                             **
 *                                                                                              **
 *  @return             The amount of data actually dropped.                                    **
 *                                                                                              **
 *************************************************************************************************/
uint32_t CircBuffDrop(circBuff_t *cbuf, uint32_t lenToDrop);

/*************************************************************************************************
 *  @brief          Returns the unread data count.                                              **
 *                                                                                              **
 *  @param cbuf     The pointer to the circular buffer.                                         **
 *                                                                                              **
 *  @return         The unread data count.                                                      **
 *                                                                                              **
 *************************************************************************************************/
uint32_t CircBuffReadSize(circBuff_t *cbuf);

/*************************************************************************************************
 *  @brief          Returns cbuf size - max data count to write with no overflow.               **
 *                                                                                              **
 *  @param cbuf     The pointer to the circular buffer.                                         **
 *                                                                                              **
 *  @return         The number of data possible to write.                                       **
 *                                                                                              **
 *************************************************************************************************/
uint32_t CircBuffWriteSize(circBuff_t *cbuf);

/*************************************************************************************************
 *  @brief          Returns cbuf size - max data count to hold in.                              **
 *                                                                                              **
 *  @param cbuf     The pointer to the circular buffer.                                         **
 *                                                                                              **
 *  @return         The size of the cbuf circular buffer.                                       **
 *                                                                                              **
 *************************************************************************************************/
uint32_t CircBuffGetTotalSize(circBuff_t *cbuf);

/*************************************************************************************************
 *  @brief          Returns true if the buffer is full.                                         **
 *                                                                                              **
 *  @param cbuf     The pointer to the circular buffer.                                         **
 *                                                                                              **
 *  @return         True if the buffer is full, otherwise false.                                **
 *                                                                                              **
 *************************************************************************************************/
bool CircBuffIsFull(circBuff_t *cbuf);

/****************************** END OF FILE  *********************************/
