/*****************************************************************************
 *
 * @file middleware/circBuff/circBuff.c
 *
 * @brief Generic circular buffer
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/

#include "middleware/CircBuff/circBuff.h"
#include "driver/AssertDebug/debug.h"

/*****************************************************************************
                               INTERFACE IMPLEMENTATION
*****************************************************************************/

bool CircBuffDynamicBufferInit(circBuff_t *cbuf, uint32_t size)
{
    ASSERT(cbuf);
    ASSERT(size);

    memset(cbuf, 0, sizeof(circBuff_t));

    cbuf->data = (uint8_t *)malloc(size);

    if (cbuf->data) {
        cbuf->size = size;
        cbuf->isAllocated = true;
        return true;
    }

    return false;
}

bool CircBuffStaticBufferInit(circBuff_t *cbuf, uint8_t *dataBuf, uint32_t size)
{
    ASSERT(cbuf);
    ASSERT(size);

    memset(cbuf, 0, sizeof(circBuff_t));

    cbuf->data = dataBuf;
    cbuf->size = size;

    return (cbuf->data != NULL);
}

void CircBuffDeinit(circBuff_t *cbuf)
{
    ASSERT(cbuf);

    if (cbuf->isAllocated) {
        free(cbuf->data);
    }

    memset(cbuf, 0, sizeof(circBuff_t));
    cbuf->data = NULL;
}

void CircBuffWrite(circBuff_t *cbuf, uint8_t *data, uint32_t length)
{
    ASSERT(cbuf);
    ASSERT(data);
    ASSERT(cbuf->size >= length);

    if (length == 0) {
        return;
    }

    uint32_t avaiableStartSpace = CircBuffWriteSize(cbuf);

    uint32_t dataNoToBuffEnd = cbuf->size - cbuf->write;

    if (length <= dataNoToBuffEnd) {
        memcpy(&cbuf->data[cbuf->write], data, length);
        cbuf->write += length;
        if (cbuf->write == cbuf->size) {
            cbuf->write = 0;
        }
    }
    else {
        memcpy(&cbuf->data[cbuf->write], data, dataNoToBuffEnd);
        uint32_t left = length - dataNoToBuffEnd;
        memcpy(cbuf->data, &data[dataNoToBuffEnd], left);
        cbuf->write = left;
    }

    // Some data was overwritten!
    if (length >= avaiableStartSpace) {
        cbuf->read = cbuf->write;
    }

    if (cbuf->read == cbuf->write) {
        cbuf->full = true;
    }
}

uint32_t CircBuffPeek(circBuff_t *cbuf, uint8_t *dataOut, uint32_t lenToRead)
{
    ASSERT(cbuf);
    ASSERT(dataOut);
    ASSERT(lenToRead <= cbuf->size);

    if (lenToRead == 0) {
        return 0;
    }

    uint32_t unreadInBuff = CircBuffReadSize(cbuf);
    uint32_t bytesToRead = (unreadInBuff < lenToRead) ? unreadInBuff : lenToRead;

    if (bytesToRead == 0) {
        return 0;
    }

    if (cbuf->write > cbuf->read) {
        memcpy(dataOut, &cbuf->data[cbuf->read], bytesToRead);
    }
    else {
        uint32_t dataNoToBuffEnd = cbuf->size - cbuf->read;
        uint32_t firstCopy = (dataNoToBuffEnd > bytesToRead) ? bytesToRead : dataNoToBuffEnd;

        if (firstCopy > 0) {
            memcpy(dataOut, &cbuf->data[cbuf->read], firstCopy);
        }

        uint32_t secondCopy = bytesToRead - firstCopy;

        if (secondCopy > 0) {
            memcpy(dataOut + firstCopy, cbuf->data, secondCopy);
        }
    }

    return bytesToRead;
}

uint32_t CircBuffDrop(circBuff_t *cbuf, uint32_t lenToDrop)
{
    ASSERT(cbuf);
    ASSERT(lenToDrop <= cbuf->size);

    if (lenToDrop == 0) {
        return 0;
    }

    uint32_t toReadCount = CircBuffReadSize(cbuf);
    if (toReadCount == 0) {
        return 0;
    }

    if (toReadCount > 0) {
        lenToDrop = lenToDrop > toReadCount ? toReadCount : lenToDrop;
        cbuf->read = ((cbuf->read + lenToDrop) > cbuf->size) ? ((cbuf->read + lenToDrop) - cbuf->size) : (cbuf->read + lenToDrop);

        cbuf->full = false;
    }

    return lenToDrop;
}

uint32_t CircBuffRead(circBuff_t *cbuf, uint8_t *dataOut, uint32_t lenToRead)
{
    ASSERT(cbuf);
    ASSERT(dataOut);
    ASSERT(lenToRead <= cbuf->size);

    if (lenToRead == 0) {
        return 0;
    }

    uint32_t justRead = CircBuffPeek(cbuf, dataOut, lenToRead);
    CircBuffDrop(cbuf, justRead);

    return justRead;
}

uint32_t CircBuffReadSize(circBuff_t *cbuf)
{
    ASSERT(cbuf);

    if (cbuf->write == cbuf->read) {
        if (cbuf->full) {
            return cbuf->size;
        }
        return 0;
    }

    if (cbuf->write > cbuf->read) {
        return cbuf->write - cbuf->read;
    }

    return cbuf->size - (cbuf->read - cbuf->write);
}

uint32_t CircBuffWriteSize(circBuff_t *cbuf)
{
    ASSERT(cbuf);

    if (cbuf->write == cbuf->read) {
        if (cbuf->full) {
            return 0;
        }
        return cbuf->size;
    }

    if (cbuf->write > cbuf->read) {
        return cbuf->size - (cbuf->write - cbuf->read);
    }

    return cbuf->read - cbuf->write;
}

uint32_t CircBuffGetTotalSize(circBuff_t *cbuf)
{
    ASSERT(cbuf);

    return cbuf->size;
}

bool CircBuffIsFull(circBuff_t *cbuf)
{
    ASSERT(cbuf);

    if (cbuf->full) {
        return true;
    }
    return false;
}


/****************************** END OF FILE  *********************************/
