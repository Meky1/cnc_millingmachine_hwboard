/*****************************************************************************
 *
 * @file middleware/CommandReception/CommandReception.c
 *
 * @brief Source file for the gcode command reception module.
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/

#include "driver/AssertDebug/debug.h"
#include "driver/CRC/crc16ccitt.h"
#include "driver/UartCommDriver/uartCommDriver.h"

#include "middleware/CommandReception/commandReception.h"
#include "middleware/StateMachine/stateMachine.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#ifdef CFG_DEBUG_MODULE_NAME
#undef CFG_DEBUG_MODULE_NAME
#endif

#define CFG_DEBUG_MODULE_NAME "[RECEIVE_COMM] "

#define RECEIVE_TIME_MS (500u)

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
 *****************************************************************************/
// clang-format off

typedef enum { 
    SM_COMM_RECEIVE_HEADER = 0, 
    SM_COMM_RECEIVE_LINE, 
    SM_COMM_RECEIVE_COMPLETE, 
    SM_COMM_RECEIVE_WAIT, 
    SM_COMM_RECEIVE_FAIL, 
    SM_COMM_RECEIVE_TIMEOUT 
} commandState_t;

// clang-format on

typedef struct PACKED {
    uint16_t crc;
    uint8_t length;
} cncCommandHeader_t;

typedef struct PACKED {
    uint16_t crc;
    uint8_t data[GCODE_LINE_LENGTH];
} cncCommandData_t;

static cncCommandHeader_t sCommandHeader = { 0 };
static cncCommandData_t sCommandData = { 0 };
static stateMachine_t sCommandStateMachine = { 0 };
static systemErrorCodes_t sErrorCode = { 0 };

static bool sIsHWBusy = false;
static bool sIsGcodeLineReady = false;

static char sGcodeLine[GCODE_LINE_LENGTH] = { 0 };

/*****************************************************************************
                            STATE MACHINE FUNCTIONS
 *****************************************************************************/

/*************************************************************************************************
 *  @brief      Receive header process.                                                         **
 *                                                                                              **
 *  @return     True if flag raised, otherwise false.                                           **
 *                                                                                              **
 *************************************************************************************************/
static stateResult_t ReceiveHeader(void);

/*************************************************************************************************
 *  @brief      Receive line process.                                                           **
 *                                                                                              **
 *  @return     State machine status (check for stateResult_t definition).                      **
 *                                                                                              **
 *************************************************************************************************/
static stateResult_t ReceiveLine(void);

/*************************************************************************************************
 *  @brief      Receive complete and cleaning process.                                          **
 *                                                                                              **
 *  @return     State machine status (check for stateResult_t definition).                      **
 *                                                                                              **
 *************************************************************************************************/
static stateResult_t ReceiveComplete(void);

/*************************************************************************************************
 *  @brief      Wait for the line to be further processed.                                      **
 *                                                                                              **
 *  @return     State machine status (check for stateResult_t definition).                      **
 *                                                                                              **
 *************************************************************************************************/
static stateResult_t ReceiveWaitForOperations(void);

/*************************************************************************************************
 *  @brief      Function process for an occurrence of a fail.                                   **
 *                                                                                              **
 *  @return     State machine status (check for stateResult_t definition).                      **
 *                                                                                              **
 *************************************************************************************************/
static stateResult_t ReceiveFail(void);

/*************************************************************************************************
 *  @brief      Function process for an occurrence of a timeout.                                **
 *                                                                                              **
 *  @return     State machine status (check for stateResult_t definition).                      **
 *                                                                                              **
 *************************************************************************************************/
static stateResult_t ReceiveTimeout(void);

static state_t sCommandStates[] = { [SM_COMM_RECEIVE_HEADER] = { "COMMAND_RECEIVE_HEADER", 0, ReceiveHeader, SM_COMM_RECEIVE_FAIL, 0 },
                                    [SM_COMM_RECEIVE_LINE] = { "COMMAND_RECEIVE_LINE", RECEIVE_TIME_MS, ReceiveLine, SM_COMM_RECEIVE_FAIL, SM_COMM_RECEIVE_TIMEOUT },
                                    [SM_COMM_RECEIVE_COMPLETE] = { "COMMAND_RECEIVE_COMPLETE", 0, ReceiveComplete, SM_COMM_RECEIVE_FAIL, 0 },
                                    [SM_COMM_RECEIVE_WAIT] = { "COMMAND_RECEIVE_WAIT", 0, ReceiveWaitForOperations, SM_COMM_RECEIVE_FAIL, 0 },
                                    [SM_COMM_RECEIVE_FAIL] = { "COMMAND_RECEIVE_FAIL", 0, ReceiveFail, SM_COMM_RECEIVE_FAIL, 0 },
                                    [SM_COMM_RECEIVE_TIMEOUT] = { "COMMAND_RECEIVE_TIMEOUT", 0, ReceiveTimeout, SM_COMM_RECEIVE_FAIL, 0 }

};

/*****************************************************************************
                         PRIVATE FUNCTION DECLARATION
 *****************************************************************************/

/*************************************************************************************************
 *  @brief      Clear the header and data structures (set them with '\0').                      **
 *                                                                                              **
 *************************************************************************************************/
static void CommandReceptionClearHeaderAndData(void);

/*************************************************************************************************
 *  @brief                  Send the command data status via UART.                              **
 *                                                                                              **
 *  @param errorCode        The command data status. For more                                   **
 *                          information check systemErrorCodes_t enum in "CncConfig.h".         **
 *                                                                                              **
 *  @return                 True if operation executed successfully, otherwise false.           **
 *                                                                                              **
 *************************************************************************************************/
static bool CommandReceptionSendStatus(systemErrorCodes_t errorCode);

/*************************************************************************************************
 *  @brief                  Set the hardware busy flag indicating that the GCode line is        **
 *                          being parsed and executed                                           **
 *                                                                                              **
 *************************************************************************************************/
static void CommandReceptionSetHWBusyFlag(void);

/*************************************************************************************************
 *  @brief                  Check if the hardware is ready for a new Gcode line.                **
 *                                                                                              **
 *  @return                 True if the hardware is ready for new data, otherwise false.        **
 *                                                                                              **
 *************************************************************************************************/
static bool CommandReceptionIsHWReadyForNewData(void);

/*****************************************************************************
                            INTERFACE IMPLEMENTATION
*****************************************************************************/

bool CommandReceptionInit(void)
{
    bool status = true;

    status &= UartCommDriverInit((uint8_t *)&sCommandHeader, sizeof(sCommandHeader));
    status &= StateMachineInit(&sCommandStateMachine, "COMMAND RECEPTION", sCommandStates, ARRAY_SIZE(sCommandStates));

    return status;
}

void CommandReceptionProcess(void)
{
    StateMachineProcess(&sCommandStateMachine);
}

const char *CommandReceptionGetGcodeLine(void)
{
    return sGcodeLine;
}

bool CommandReceptionIsGcodeLineReady(void)
{
    return sIsGcodeLineReady;
}

void CommandReceptionClearHWBusyFlag(void)
{
    sIsHWBusy = false;
}

/******************************************************************************
                      STATE MACHINE FUNCTION IMPLEMENTATION
 ******************************************************************************/

static stateResult_t ReceiveHeader(void)
{
    static bool isSetupReady = false;

    if (!isSetupReady) {

        /* Cleanup after transmission.
        ****************************************************************/
        UartCommDriverReceiveFlagClear();
        CommandReceptionClearHeaderAndData();

        isSetupReady = true;
    }

    /* Wait until header is fully received.
    ****************************************************************/
    if (UartCommDriverGetReceiveFlagStatus()) {

        /* Calculate CRC of the header.
        ****************************************************************/
        if (CrcCalculate16bCRC(&sCommandHeader.length, sizeof(sCommandHeader.length)) != sCommandHeader.crc) {
            sErrorCode = COMMAND_CRC_FAILURE;
            return STATE_MACHINE_ERROR;
        }

        /* Send confirmation to the CNC CPU that the header has been received successfully.
        ****************************************************************/
        CommandReceptionSendStatus(COMMAND_HEADER_OK);

        /* Clear receive flag, begin the reception of the data line.
        ****************************************************************/
        UartCommDriverReceiveFlagClear();
        UartCommDriverReceiveIT((uint8_t *)&sCommandData, sCommandHeader.length + sizeof(sCommandData.crc));

        /* Change to the line reception state.
        ****************************************************************/
        StateMachineChangeState(&sCommandStateMachine, SM_COMM_RECEIVE_LINE);
        isSetupReady = false;
    }

    return STATE_MACHINE_OK;
}

static stateResult_t ReceiveLine(void)
{
    /* Wait until the data line is fully received.
    ****************************************************************/
    if (UartCommDriverGetReceiveFlagStatus()) {

        /* Calculate CRC of the data line.
        ****************************************************************/
        if (CrcCalculate16bCRC(sCommandData.data, sizeof(sCommandData.data)) != sCommandData.crc) {
            sErrorCode = COMMAND_CRC_FAILURE;
            return STATE_MACHINE_ERROR;
        }

        /* Send confirmation to the CNC CPU that the header has been received successfully.
        ****************************************************************/
        CommandReceptionSendStatus(COMMAND_DATA_OK);

        /* Change to the complete and cleanup state.
        ****************************************************************/
        StateMachineChangeState(&sCommandStateMachine, SM_COMM_RECEIVE_COMPLETE);
    }

    return STATE_MACHINE_OK;
}

static stateResult_t ReceiveComplete(void)
{
    LOG_INFO("Command received.");

    /* Copy the GCode line from the sCommandData to static buffer.
    ****************************************************************/
    memcpy(sGcodeLine, sCommandData.data, GCODE_LINE_LENGTH);
    sIsGcodeLineReady = true;

    /* Raise busy flag for command line parsing and execution.
    ****************************************************************/
    CommandReceptionSetHWBusyFlag();

    /* Cleanup after transmission.
    ****************************************************************/
    UartCommDriverReceiveFlagClear();
    CommandReceptionClearHeaderAndData();

    /* Change to the header state.
    ****************************************************************/
    StateMachineChangeState(&sCommandStateMachine, SM_COMM_RECEIVE_WAIT);

    return STATE_MACHINE_OK;
}

static stateResult_t ReceiveWaitForOperations(void)
{
    LOG_INFO("Waiting for the line to be further processed...");

    if (CommandReceptionIsHWReadyForNewData()) {

        /* Change to the header state.
        ****************************************************************/
        sIsGcodeLineReady = false;
        StateMachineChangeState(&sCommandStateMachine, SM_COMM_RECEIVE_HEADER);
    }

    return STATE_MACHINE_OK;
}

static stateResult_t ReceiveFail(void)
{
    /* Display error connected message and send it to the CNC CPU.
    ****************************************************************/
    switch (sErrorCode) {
    case COMMAND_CRC_FAILURE: {
        CommandReceptionSendStatus(sErrorCode);
        LOG_ERROR("Failed to receive command. CRC failure.");
        break;
    }

        // TODO: Add different error exceptions.

    default: {
        LOG_ERROR("Unknown failure.");
        break;
    }
    }

    /* Cleanup after transmission fail.
    ****************************************************************/
    UartCommDriverReceiveFlagClear();
    CommandReceptionClearHeaderAndData();

    /* Change to the header state.
    ****************************************************************/
    StateMachineChangeState(&sCommandStateMachine, SM_COMM_RECEIVE_HEADER);

    return STATE_MACHINE_OK;
}

static stateResult_t ReceiveTimeout(void)
{
    /* Send timeout message to the CNC CPU.
    ****************************************************************/
    CommandReceptionSendStatus(COMMAND_TRANSMISSION_TIMEOUT);

    /* Display error connected message.
    ****************************************************************/
    LOG_ERROR("Failed to receive command. Timeout %dms exceeded.", RECEIVE_TIME_MS);

    /* Cleanup after transmission fail.
    ****************************************************************/
    UartCommDriverReceiveFlagClear();
    CommandReceptionClearHeaderAndData();

    /* Change to the header state.
    ****************************************************************/
    StateMachineChangeState(&sCommandStateMachine, SM_COMM_RECEIVE_HEADER);

    return STATE_MACHINE_OK;
}

/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
 ******************************************************************************/

static void CommandReceptionClearHeaderAndData(void)
{
    memset(&sCommandHeader, 0, sizeof(sCommandHeader));
    memset(&sCommandData, 0, sizeof(sCommandData));
}

static bool CommandReceptionSendStatus(systemErrorCodes_t errorCode)
{
    ASSERT(errorCode > COMMAND_HEADER_OK);
    ASSERT(errorCode <= COMMAND_READY);

    bool status = false;

    status = UartCommDriverTransmit((uint8_t *)&errorCode, 1u);

    return status;
}

static void CommandReceptionSetHWBusyFlag(void)
{
    sIsHWBusy = true;
}

static bool CommandReceptionIsHWReadyForNewData(void)
{
    return !sIsHWBusy;
}

/****************************** END OF FILE  *********************************/
