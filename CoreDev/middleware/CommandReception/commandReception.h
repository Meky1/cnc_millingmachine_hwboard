/*****************************************************************************
 *
 * @file middleware/CommandReception/CommandReception.h
 *
 * @brief Header file for the gcode command reception module.
 *
 *
 * @author Marcin Czarnik
 * @date 13.07.2022
 * @version v1.0
 *
 ****************************************************************************/

#pragma once

#include "CncConfig.h"

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
*****************************************************************************/


/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
*****************************************************************************/

/*************************************************************************************************
 *  @brief              Initialize the command reception system.                                **
 *  @return             True if system initialized successfully, otherwise false.               **
 *                                                                                              **
 *************************************************************************************************/
bool CommandReceptionInit(void);

/*************************************************************************************************
 *  @brief              Command reception process.                                              **
 *                                                                                              **
 *************************************************************************************************/
void CommandReceptionProcess(void);

/*************************************************************************************************
 *  @brief              Get the received GCode line.                                            **
 *  @return             The GCode line in string format.                                        **
 *                                                                                              **
 *************************************************************************************************/
const char *CommandReceptionGetGcodeLine(void);

/*************************************************************************************************
 *  @brief              Check if the GCode line is received and ready to parse.                 **
 *                                                                                              **
 *************************************************************************************************/
bool CommandReceptionIsGcodeLineReady(void);

/*************************************************************************************************
 *  @brief              Clear the hardware busy flag indicating that the command reception      **
 *                      system can be initialized once more                                     **
 *                                                                                              **
 *************************************************************************************************/
void CommandReceptionClearHWBusyFlag(void);

/****************************** END OF FILE  *********************************/
