mkdir build
cd build
cmake .. -G "MinGW Makefiles"
cd ..
cmake --build build -j16
cd build; ctest -VV; cd ..
