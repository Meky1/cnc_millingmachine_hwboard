
/*****************************************************************************
                                MOCKING HEADER
 *****************************************************************************/
#include "driver/TimeBaseDriver/timeBaseDriver.h"

/*****************************************************************************
                               PRIVATE DEFINES
 *****************************************************************************/
#define TIMEOUT_SIMULATE_MS (50u)

/*****************************************************************************
                               MOCKED FUNCTIONS
 *****************************************************************************/

uint32_t TimeBaseDriverGetTick(void)
{
    /* Create static variable used to simulate a 50ms timeout.
    ****************************************************************/   
    static uint32_t x = 0u;
    return x += TIMEOUT_SIMULATE_MS;
}

bool TimeBaseDriverHasTimeElapsed(uint32_t startTime, uint32_t deltaMsTime)
{
    (void)startTime;
    (void)deltaMsTime;

    return true;
}

void DelayMs(uint32_t delayTime)
{
    (void)delayTime;
}
