
/*****************************************************************************
                           INCLUDE GTEST FRAMEWORK
 *****************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <stdio.h>

/*****************************************************************************
                          INCLUDE TESTED SOURCE FILES
 *****************************************************************************/

extern "C" {
#include "driver/CRC/crc16ccitt.h"
}

/*****************************************************************************
                         TEST SUITE SETUP AND TEAR DOWN
*****************************************************************************/

class CrcTest : public testing::Test {
protected:
    void SetUp() override
    {
    }

    void TearDown() override
    {
    }
};

/*****************************************************************************
                                TEST FUNCTIONS
 *****************************************************************************/

TEST(CrcTest, BasicOperationA)
{
    /* Input variables.
    ****************************************************************/
    uint8_t data[] = { 0x66, 0x55, 0x73, 0x21, 0x98, 0x21 };
    uint16_t expectedResult = 0x29C2;

    /* Test.
    ****************************************************************/
    uint16_t result = CrcCalculate16bCRC(data, sizeof(data));
    EXPECT_EQ(result, expectedResult);
}

TEST(CrcTest, BasicOperationB)
{
    /* Input variables.
    ****************************************************************/
    uint16_t data[] = { 0x5566, 0x2173, 0x2198 };
    uint16_t expectedResult = 0x29C2;

    /* Test.
    ****************************************************************/
    uint16_t result = CrcCalculate16bCRC((uint8_t *)&data, sizeof(data));
    EXPECT_EQ(result, expectedResult);
}

TEST(CrcTest, AdvancedOperationA)
{
    /* Input variables.
    ****************************************************************/
    uint16_t data[] = { 0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823, 0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b, 0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12 };
    uint16_t expectedResult = 0x0553;

    /* Test.
    ****************************************************************/
    uint16_t result = CrcCalculate16bCRC((uint8_t *)&data, sizeof(data));
    EXPECT_EQ(result, expectedResult);
}
