
/*****************************************************************************
                           INCLUDE GTEST FRAMEWORK
 *****************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <stdio.h>

/*****************************************************************************
                          INCLUDE TESTED SOURCE FILES
 *****************************************************************************/
extern "C" {
#include "middleware/StateMachine/stateMachine.h"
}

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

typedef enum {
    SM_TEST_STATE_0 = 0,
    SM_TEST_STATE_1,
    SM_TEST_STATE_2,
    SM_TEST_STATE_3,
    SM_TEST_STATE_4,
    SM_TEST_STATE_5,

    SM_TEST_STATE_FAIL,
    SM_TEST_STATE_TIMEOUT
} commandState_t;

/*****************************************************************************
                         TEST SUITE SETUP AND TEAR DOWN
*****************************************************************************/

class StateMachineTest : public ::testing::Test {
protected:
    void SetUp() override
    {
    }

    void TearDown() override
    {
    }
};

/*****************************************************************************
                            STATE MACHINE SETUP
 *****************************************************************************/

/* State machine declaration to be tested.
****************************************************************/
static stateMachine_t sStateMachineTest;
static bool sErrorFlag = false;
static bool sTimeoutFlag = false;

/* State machine function states definitions.
****************************************************************/
static stateResult_t StateMachineTestState0(void);
static stateResult_t StateMachineTestState1(void);
static stateResult_t StateMachineTestState2(void);
static stateResult_t StateMachineTestState3(void);
static stateResult_t StateMachineTestState4(void);
static stateResult_t StateMachineTestState5(void);
static stateResult_t StateMachineTestStateFail(void);
static stateResult_t StateMachineTestStateTimeout(void);

/* State machine states definitions.
****************************************************************/
state_t sStateMachineTestStates[] = { [SM_TEST_STATE_0] = { "SM_TEST_STATE_0", 0, StateMachineTestState0, SM_TEST_STATE_FAIL, SM_TEST_STATE_TIMEOUT },          [SM_TEST_STATE_1] = { "SM_TEST_STATE_1", 0, StateMachineTestState1, SM_TEST_STATE_FAIL, SM_TEST_STATE_TIMEOUT },
                                      [SM_TEST_STATE_2] = { "SM_TEST_STATE_2", 0, StateMachineTestState2, SM_TEST_STATE_FAIL, SM_TEST_STATE_TIMEOUT },          [SM_TEST_STATE_3] = { "SM_TEST_STATE_3", 0, StateMachineTestState3, SM_TEST_STATE_FAIL, SM_TEST_STATE_TIMEOUT },
                                      [SM_TEST_STATE_4] = { "SM_TEST_STATE_4", 51, StateMachineTestState3, SM_TEST_STATE_FAIL, SM_TEST_STATE_TIMEOUT },         [SM_TEST_STATE_5] = { "SM_TEST_STATE_5", 49, StateMachineTestState3, SM_TEST_STATE_FAIL, SM_TEST_STATE_TIMEOUT },
                                      [SM_TEST_STATE_FAIL] = { "SM_TEST_STATE_FAIL", 0, StateMachineTestStateFail, SM_TEST_STATE_FAIL, SM_TEST_STATE_TIMEOUT }, [SM_TEST_STATE_TIMEOUT] = { "SM_TEST_STATE_TIMEOUT", 0, StateMachineTestStateTimeout, SM_TEST_STATE_FAIL, SM_TEST_STATE_TIMEOUT } };

/* State machine states declaration.
****************************************************************/

static stateResult_t StateMachineTestState0(void)
{
    StateMachineChangeState(&sStateMachineTest, SM_TEST_STATE_1);
    return STATE_MACHINE_OK;
}

static stateResult_t StateMachineTestState1(void)
{
    return STATE_MACHINE_NEXT_STATE;
}

static stateResult_t StateMachineTestState2(void)
{
    StateMachineChangeState(&sStateMachineTest, SM_TEST_STATE_3);
    return STATE_MACHINE_OK;
}

static stateResult_t StateMachineTestState3(void)
{
    return STATE_MACHINE_ERROR;
}

static stateResult_t StateMachineTestState4(void)
{
    return STATE_MACHINE_OK;
}

static stateResult_t StateMachineTestState5(void)
{
    return STATE_MACHINE_OK;
}

static stateResult_t StateMachineTestStateFail(void)
{
    sErrorFlag = true;
    return STATE_MACHINE_OK;
}

static stateResult_t StateMachineTestStateTimeout(void)
{
    sTimeoutFlag = true;
    return STATE_MACHINE_OK;
}

/*****************************************************************************
                                TEST FUNCTIONS
 *****************************************************************************/

TEST(StateMachineTest, StateMachineInitTest)
{
    /* Initialize the state machine.
    ****************************************************************/
    bool status = StateMachineInit(&sStateMachineTest, "STATE MACHINE TEST", sStateMachineTestStates, ARRAY_SIZE(sStateMachineTestStates));
    ASSERT_EQ(status, true);

    /* Check the starting state name and ID.
    ****************************************************************/
    const char *stateName = StateMachineGetCurrentStateName(&sStateMachineTest);
    uint32_t stateId = StateMachineGetCurrentStateId(&sStateMachineTest);

    EXPECT_STREQ(stateName, "SM_TEST_STATE_0");
    EXPECT_EQ(stateId, SM_TEST_STATE_0);

    /* Clear the state machine test object for future testing.
    ****************************************************************/
    memset(&sStateMachineTest, 0, sizeof(sStateMachineTest));
}

TEST(StateMachineTest, StateMachineProcessTest)
{
    /* Initialize the state machine.
    ****************************************************************/
    bool status = StateMachineInit(&sStateMachineTest, "STATE MACHINE TEST", sStateMachineTestStates, ARRAY_SIZE(sStateMachineTestStates));
    ASSERT_EQ(status, true);

    /* Execute state machine process.
    ****************************************************************/
    StateMachineProcess(&sStateMachineTest);

    /* Check the current state name and ID.
    ****************************************************************/
    const char *stateName = StateMachineGetCurrentStateName(&sStateMachineTest);
    uint32_t stateId = StateMachineGetCurrentStateId(&sStateMachineTest);

    EXPECT_STREQ(stateName, "SM_TEST_STATE_1");
    EXPECT_EQ(stateId, SM_TEST_STATE_1);

    /* Execute state machine process.
    ****************************************************************/
    StateMachineProcess(&sStateMachineTest);

    /* Check the current state name and ID.
    ****************************************************************/
    stateName = StateMachineGetCurrentStateName(&sStateMachineTest);
    stateId = StateMachineGetCurrentStateId(&sStateMachineTest);

    EXPECT_STREQ(stateName, "SM_TEST_STATE_2");
    EXPECT_EQ(stateId, SM_TEST_STATE_2);

    /* Clear the state machine test object for future testing.
    ****************************************************************/
    memset(&sStateMachineTest, 0, sizeof(sStateMachineTest));
}

TEST(StateMachineTest, StateMachineErrorOccurrenceTest)
{
    /* Initialize the state machine.
    ****************************************************************/
    bool status = StateMachineInit(&sStateMachineTest, "STATE MACHINE TEST", sStateMachineTestStates, ARRAY_SIZE(sStateMachineTestStates));
    ASSERT_EQ(status, true);

    /* Execute state machine process multiple times.
    ****************************************************************/
    for (uint8_t i = 0u; i <= SM_TEST_STATE_3; i++) {
        StateMachineProcess(&sStateMachineTest);
    }

    /* Check the current state name and ID.
    ****************************************************************/
    const char *stateName = StateMachineGetCurrentStateName(&sStateMachineTest);
    uint32_t stateId = StateMachineGetCurrentStateId(&sStateMachineTest);

    EXPECT_STREQ(stateName, "SM_TEST_STATE_FAIL");
    EXPECT_EQ(stateId, SM_TEST_STATE_FAIL);

    /* Execute state machine process for a single function execution.
    ****************************************************************/
    StateMachineProcess(&sStateMachineTest);
    EXPECT_EQ(sErrorFlag, true);

    /* Clear the state machine test object for future testing.
    ****************************************************************/
    memset(&sStateMachineTest, 0, sizeof(sStateMachineTest));
    sErrorFlag = false;
}

TEST(StateMachineTest, StateMachineTimeoutOccurrenceTest)
{
    /* Initialize the state machine.
    ****************************************************************/
    bool status = StateMachineInit(&sStateMachineTest, "STATE MACHINE TEST", sStateMachineTestStates, ARRAY_SIZE(sStateMachineTestStates));
    ASSERT_EQ(status, true);

    /* Execute state machine process with longer timeout than 50ms.
    ****************************************************************/
    StateMachineChangeState(&sStateMachineTest, SM_TEST_STATE_4);
    StateMachineProcess(&sStateMachineTest);
    EXPECT_EQ(sTimeoutFlag, false);

    /* Execute state machine process with shorter timeout than 50ms.
    ****************************************************************/
    StateMachineChangeState(&sStateMachineTest, SM_TEST_STATE_5);
    StateMachineProcess(&sStateMachineTest);
    EXPECT_EQ(sTimeoutFlag, true);

    /* Clear the state machine test object for future testing.
    ****************************************************************/
    memset(&sStateMachineTest, 0, sizeof(sStateMachineTest));
    sTimeoutFlag = false;
}
