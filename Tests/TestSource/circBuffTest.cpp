
/*****************************************************************************
                           INCLUDE GTEST FRAMEWORK
 *****************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

/*****************************************************************************
                          INCLUDE TESTED SOURCE FILES
 *****************************************************************************/

extern "C" {
#include "middleware/CircBuff/circBuff.h"
}

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

#define DATA_SIZE_LONG (60u)
#define DATA_SIZE_MEDIUM (20u)
#define DATA_SIZE_SHORT (10u)
#define DATA_SIZE_VERY_SHORT (5u)

/*****************************************************************************
                         TEST SUITE SETUP AND TEAR DOWN
*****************************************************************************/

class CircBuffTest : public testing::Test {
protected:
    void SetUp() override
    {
    }

    void TearDown() override
    {
    }
};

TEST(CircBuffTest, DynamicBufferInit)
{
    /* Input variables.
    ****************************************************************/
    circBuff_t dynamicCircularBuffer = { 0 };

    /* Test.
    ****************************************************************/
    bool initStatus = CircBuffDynamicBufferInit(&dynamicCircularBuffer, DATA_SIZE_MEDIUM);

    EXPECT_EQ(initStatus, true);
    EXPECT_EQ(dynamicCircularBuffer.size, DATA_SIZE_MEDIUM);
    EXPECT_EQ(dynamicCircularBuffer.isAllocated, true);

    CircBuffDeinit(&dynamicCircularBuffer);

    EXPECT_EQ(dynamicCircularBuffer.isAllocated, false);
    EXPECT_EQ(dynamicCircularBuffer.data, (uint8_t *)NULL);
}

TEST(CircBuffTest, StaticBufferInit)
{
    /* Input variables.
    ****************************************************************/
    uint32_t data[DATA_SIZE_MEDIUM] = { 0 };
    circBuff_t staticCircularBuffer = { 0 };

    /* Test.
    ****************************************************************/
    bool status = CircBuffStaticBufferInit(&staticCircularBuffer, (uint8_t *)&data, DATA_SIZE_MEDIUM * sizeof(uint32_t));

    EXPECT_EQ(staticCircularBuffer.size, DATA_SIZE_MEDIUM * sizeof(uint32_t));
    EXPECT_EQ(status, true);
}

TEST(CircBuffTest, DynamicBufferWrite)
{
    /* Input variables.
    ****************************************************************/
    circBuff_t dynamicCircularBuffer = { 0 };
    uint8_t dataBuffer[DATA_SIZE_MEDIUM] = { 0 };

    uint8_t dataToWriteA[DATA_SIZE_SHORT] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A };
    uint8_t dataToWriteB[DATA_SIZE_SHORT] = { 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14 };
    uint8_t dataToWriteC[DATA_SIZE_VERY_SHORT] = { 0x70, 0x71, 0x72, 0x73, 0x74 };

    /* Test vectors.
    ****************************************************************/
    uint8_t testBufferA[DATA_SIZE_MEDIUM] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    uint8_t testBufferB[DATA_SIZE_MEDIUM] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14 };

    uint8_t testBufferC[DATA_SIZE_MEDIUM] = { 0x70, 0x71, 0x72, 0x73, 0x74, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14 };

    /* Test.
    ****************************************************************/
    bool initStatus = CircBuffDynamicBufferInit(&dynamicCircularBuffer, DATA_SIZE_MEDIUM);

    CircBuffWrite(&dynamicCircularBuffer, dataToWriteA, DATA_SIZE_SHORT);

    for (int i = 0; i < DATA_SIZE_SHORT; i++) {
        EXPECT_EQ(dynamicCircularBuffer.data[i], testBufferA[i]);
    }

    CircBuffWrite(&dynamicCircularBuffer, dataToWriteB, DATA_SIZE_SHORT);

    for (int i = 0; i < DATA_SIZE_MEDIUM; i++) {
        EXPECT_EQ(dynamicCircularBuffer.data[i], testBufferB[i]);
    }

    CircBuffWrite(&dynamicCircularBuffer, dataToWriteC, DATA_SIZE_VERY_SHORT);

    for (int i = 0; i < DATA_SIZE_MEDIUM; i++) {
        EXPECT_EQ(dynamicCircularBuffer.data[i], testBufferC[i]);
    }
}

TEST(CircBuffTest, StaticBufferWrite)
{
    /* Input variables.
    ****************************************************************/
    circBuff_t staticCircularBuffer = { 0 };
    uint8_t dataBuffer[DATA_SIZE_MEDIUM] = { 0 };

    uint8_t dataToWriteA[DATA_SIZE_SHORT] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A };
    uint8_t dataToWriteB[DATA_SIZE_SHORT] = { 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14 };
    uint8_t dataToWriteC[DATA_SIZE_VERY_SHORT] = { 0x70, 0x71, 0x72, 0x73, 0x74 };

    /* Test vectors.
    ****************************************************************/
    uint8_t testBufferA[DATA_SIZE_MEDIUM] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    uint8_t testBufferB[DATA_SIZE_MEDIUM] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14 };

    uint8_t testBufferC[DATA_SIZE_MEDIUM] = { 0x70, 0x71, 0x72, 0x73, 0x74, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14 };

    /* Test.
    ****************************************************************/
    bool initStatus = CircBuffStaticBufferInit(&staticCircularBuffer, dataBuffer, DATA_SIZE_MEDIUM);

    CircBuffWrite(&staticCircularBuffer, dataToWriteA, DATA_SIZE_SHORT);

    for (int i = 0; i < DATA_SIZE_SHORT; i++) {
        EXPECT_EQ(staticCircularBuffer.data[i], testBufferA[i]);
    }

    CircBuffWrite(&staticCircularBuffer, dataToWriteB, DATA_SIZE_SHORT);

    for (int i = 0; i < DATA_SIZE_MEDIUM; i++) {
        EXPECT_EQ(staticCircularBuffer.data[i], testBufferB[i]);
    }

    CircBuffWrite(&staticCircularBuffer, dataToWriteC, DATA_SIZE_VERY_SHORT);

    for (int i = 0; i < DATA_SIZE_MEDIUM; i++) {
        EXPECT_EQ(staticCircularBuffer.data[i], testBufferC[i]);
    }
}

TEST(CircBuffTest, BufferPeek)
{
    /* Input variables.
    ****************************************************************/
    circBuff_t circularBuffer = { 0 };
    uint8_t dataBuffer[DATA_SIZE_MEDIUM] = { 0 };
    uint8_t dataOutBuffer[DATA_SIZE_MEDIUM] = { 0 };

    uint8_t writeBufferA[DATA_SIZE_MEDIUM] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14 };
    uint8_t writeBufferB[DATA_SIZE_VERY_SHORT] = { 0x70, 0x71, 0x72, 0x73, 0x74 };

    /* Test vectors.
    ****************************************************************/
    uint8_t testBufferA[DATA_SIZE_MEDIUM] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14 };

    uint8_t testBufferB[DATA_SIZE_MEDIUM] = { 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14, 0x70, 0x71, 0x72, 0x73, 0x74 };

    uint8_t testBufferC[DATA_SIZE_MEDIUM] = { 0x70, 0x71, 0x72, 0x73, 0x74, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14 };

    /* Test.
    ****************************************************************/
    CircBuffStaticBufferInit(&circularBuffer, dataBuffer, DATA_SIZE_MEDIUM);
    CircBuffWrite(&circularBuffer, writeBufferA, DATA_SIZE_MEDIUM);
    CircBuffPeek(&circularBuffer, dataOutBuffer, DATA_SIZE_MEDIUM);

    for (int i = 0; i < DATA_SIZE_MEDIUM; i++) {
        EXPECT_EQ(dataOutBuffer[i], testBufferA[i]);
        EXPECT_EQ(circularBuffer.data[i], testBufferA[i]);
    }

    memset(dataOutBuffer, 0, DATA_SIZE_MEDIUM);
    CircBuffWrite(&circularBuffer, writeBufferB, DATA_SIZE_VERY_SHORT);
    CircBuffPeek(&circularBuffer, dataOutBuffer, DATA_SIZE_VERY_SHORT);

    for (int i = 0; i < DATA_SIZE_VERY_SHORT; i++) {
        EXPECT_EQ(dataOutBuffer[i], testBufferB[i]);
        EXPECT_EQ(circularBuffer.data[i], testBufferC[i]);
    }
}

TEST(CircBuffTest, BufferDrop)
{
    /* Input variables.
    ****************************************************************/
    circBuff_t circularBuffer = { 0 };
    uint8_t dataBuffer[DATA_SIZE_MEDIUM] = { 0 };
    uint8_t dataOutBuffer[DATA_SIZE_MEDIUM] = { 0 };

    uint8_t writeBufferA[DATA_SIZE_MEDIUM] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14 };

    uint8_t writeBufferB[DATA_SIZE_VERY_SHORT] = { 0x70, 0x71, 0x72, 0x73, 0x74 };

    /* Test vectors.
    ****************************************************************/

    uint8_t testBufferA[DATA_SIZE_MEDIUM] = { 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00 };

    uint8_t testBufferB[DATA_SIZE_MEDIUM] = { 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14, 0x70, 0x71, 0x72, 0x73, 0x74, 0x00, 0x00, 0x00, 0x00, 0x00 };

    /* Test.
    ****************************************************************/
    CircBuffStaticBufferInit(&circularBuffer, dataBuffer, DATA_SIZE_MEDIUM);

    CircBuffWrite(&circularBuffer, writeBufferA, DATA_SIZE_MEDIUM);
    CircBuffDrop(&circularBuffer, DATA_SIZE_MEDIUM);
    CircBuffPeek(&circularBuffer, dataOutBuffer, DATA_SIZE_VERY_SHORT);

    for (int i = 0; i < DATA_SIZE_MEDIUM; i++) {
        EXPECT_EQ(dataOutBuffer[i], 0x00);
    }

    memset(dataOutBuffer, 0, DATA_SIZE_MEDIUM);

    CircBuffWrite(&circularBuffer, writeBufferA, DATA_SIZE_MEDIUM);
    CircBuffDrop(&circularBuffer, DATA_SIZE_VERY_SHORT);
    CircBuffPeek(&circularBuffer, dataOutBuffer, DATA_SIZE_MEDIUM);

    for (int i = 0; i < DATA_SIZE_MEDIUM; i++) {
        EXPECT_EQ(dataOutBuffer[i], testBufferA[i]);
    }

    memset(dataOutBuffer, 0, DATA_SIZE_MEDIUM);

    CircBuffWrite(&circularBuffer, writeBufferA, DATA_SIZE_MEDIUM);
    CircBuffDrop(&circularBuffer, DATA_SIZE_SHORT);
    CircBuffWrite(&circularBuffer, writeBufferB, DATA_SIZE_VERY_SHORT);
    CircBuffPeek(&circularBuffer, dataOutBuffer, DATA_SIZE_MEDIUM);

    for (int i = 0; i < DATA_SIZE_MEDIUM; i++) {
        EXPECT_EQ(dataOutBuffer[i], testBufferB[i]);
    }
}

TEST(CircBuffTest, BufferRead)
{
    /* Input variables.
    ****************************************************************/
    circBuff_t circularBuffer = { 0 };
    uint8_t dataBuffer[DATA_SIZE_MEDIUM] = { 0 };
    uint8_t dataOutBuffer[DATA_SIZE_MEDIUM] = { 0 };

    uint8_t writeBufferA[DATA_SIZE_MEDIUM] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14 };
    uint8_t writeBufferB[DATA_SIZE_VERY_SHORT] = { 0x70, 0x71, 0x72, 0x73, 0x74 };

    /* Test vectors.
    ****************************************************************/
    uint8_t testBufferA[DATA_SIZE_MEDIUM] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14 };

    uint8_t testBufferB[DATA_SIZE_MEDIUM] = { 0x70, 0x71, 0x72, 0x73, 0x74, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    uint8_t testBufferC[DATA_SIZE_MEDIUM] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    /* Test.
    ****************************************************************/
    CircBuffStaticBufferInit(&circularBuffer, dataBuffer, DATA_SIZE_MEDIUM);

    CircBuffWrite(&circularBuffer, writeBufferA, DATA_SIZE_MEDIUM);
    CircBuffRead(&circularBuffer, dataOutBuffer, DATA_SIZE_MEDIUM);
    for (int i = 0; i < DATA_SIZE_MEDIUM; i++) {
        EXPECT_EQ(dataOutBuffer[i], testBufferA[i]);
        EXPECT_EQ(circularBuffer.data[i], testBufferA[i]);
    }

    memset(dataOutBuffer, 0, DATA_SIZE_MEDIUM);
    CircBuffRead(&circularBuffer, dataOutBuffer, DATA_SIZE_MEDIUM);
    for (int i = 0; i < DATA_SIZE_MEDIUM; i++) {
        EXPECT_EQ(dataOutBuffer[i], testBufferC[i]);
    }

    memset(dataOutBuffer, 0, DATA_SIZE_MEDIUM);
    CircBuffWrite(&circularBuffer, writeBufferB, DATA_SIZE_VERY_SHORT);
    CircBuffRead(&circularBuffer, dataOutBuffer, DATA_SIZE_VERY_SHORT);
    for (int i = 0; i < DATA_SIZE_MEDIUM; i++) {
        EXPECT_EQ(dataOutBuffer[i], testBufferB[i]);
    }

    memset(dataOutBuffer, 0, DATA_SIZE_MEDIUM);
    CircBuffRead(&circularBuffer, dataOutBuffer, DATA_SIZE_MEDIUM);
    for (int i = 0; i < DATA_SIZE_MEDIUM; i++) {
        EXPECT_EQ(dataOutBuffer[i], testBufferC[i]);
    }
}

TEST(CircBuffTest, BufferReadSize)
{
    /* Input variables.
    ****************************************************************/
    circBuff_t circularBuffer = { 0 };
    uint32_t bufferSize = 0;
    uint8_t dataBuffer[DATA_SIZE_MEDIUM] = { 0 };
    uint8_t writeBufferA[DATA_SIZE_MEDIUM] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14 };
    uint8_t writeBufferB[DATA_SIZE_VERY_SHORT] = { 0x70, 0x71, 0x72, 0x73, 0x74 };

    /* Test.
    ****************************************************************/
    CircBuffStaticBufferInit(&circularBuffer, dataBuffer, DATA_SIZE_MEDIUM);

    CircBuffWrite(&circularBuffer, writeBufferA, DATA_SIZE_MEDIUM);
    bufferSize = CircBuffReadSize(&circularBuffer);
    EXPECT_EQ(bufferSize, DATA_SIZE_MEDIUM);
    CircBuffDrop(&circularBuffer, DATA_SIZE_MEDIUM);

    CircBuffWrite(&circularBuffer, writeBufferB, DATA_SIZE_VERY_SHORT);
    bufferSize = CircBuffReadSize(&circularBuffer);
    EXPECT_EQ(bufferSize, DATA_SIZE_VERY_SHORT);
    CircBuffDrop(&circularBuffer, 1u);

    bufferSize = CircBuffReadSize(&circularBuffer);
    EXPECT_EQ(bufferSize, DATA_SIZE_VERY_SHORT - 1u);
    CircBuffDrop(&circularBuffer, DATA_SIZE_MEDIUM);

    CircBuffWrite(&circularBuffer, writeBufferA, DATA_SIZE_MEDIUM);
    CircBuffWrite(&circularBuffer, writeBufferB, DATA_SIZE_VERY_SHORT);
    CircBuffDrop(&circularBuffer, 1u);
    bufferSize = CircBuffReadSize(&circularBuffer);
    EXPECT_EQ(bufferSize, DATA_SIZE_MEDIUM - 1u);
}

TEST(CircBuffTest, BufferWriteSize)
{
    /* Input variables.
    ****************************************************************/
    circBuff_t circularBuffer = { 0 };
    uint32_t bufferWriteSize = 0;
    uint8_t dataBuffer[DATA_SIZE_MEDIUM] = { 0 };

    uint8_t writeBufferA[DATA_SIZE_MEDIUM] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14 };
    uint8_t writeBufferB[DATA_SIZE_VERY_SHORT] = { 0x70, 0x71, 0x72, 0x73, 0x74 };

    /* Test.
    ****************************************************************/
    CircBuffStaticBufferInit(&circularBuffer, dataBuffer, DATA_SIZE_MEDIUM);

    CircBuffWrite(&circularBuffer, writeBufferA, DATA_SIZE_MEDIUM);
    bufferWriteSize = CircBuffWriteSize(&circularBuffer);
    EXPECT_EQ(bufferWriteSize, 0);
    CircBuffDrop(&circularBuffer, DATA_SIZE_MEDIUM);

    CircBuffWrite(&circularBuffer, writeBufferB, DATA_SIZE_VERY_SHORT);
    bufferWriteSize = CircBuffWriteSize(&circularBuffer);
    EXPECT_EQ(bufferWriteSize, DATA_SIZE_MEDIUM - DATA_SIZE_VERY_SHORT);
    CircBuffDrop(&circularBuffer, 1u);
    bufferWriteSize = CircBuffWriteSize(&circularBuffer);
    EXPECT_EQ(bufferWriteSize, DATA_SIZE_MEDIUM - DATA_SIZE_VERY_SHORT + 1u);
    CircBuffDrop(&circularBuffer, DATA_SIZE_MEDIUM);

    CircBuffWrite(&circularBuffer, writeBufferA, DATA_SIZE_MEDIUM);
    CircBuffWrite(&circularBuffer, writeBufferB, DATA_SIZE_VERY_SHORT);
    CircBuffDrop(&circularBuffer, 1u);
    bufferWriteSize = CircBuffWriteSize(&circularBuffer);
    EXPECT_EQ(bufferWriteSize, 1u);
}

TEST(CircBuffTest, BufferGetTotalSize)
{
    /* Input variables.
    ****************************************************************/
    circBuff_t circularBuffer = { 0 };
    uint32_t totalSize = 0;
    uint8_t dataBuffer[DATA_SIZE_MEDIUM] = { 0 };

    uint8_t writeBuffer[DATA_SIZE_MEDIUM] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14 };

    /* Test.
    ****************************************************************/
    CircBuffStaticBufferInit(&circularBuffer, dataBuffer, DATA_SIZE_MEDIUM);

    CircBuffWrite(&circularBuffer, writeBuffer, DATA_SIZE_MEDIUM);

    totalSize = CircBuffGetTotalSize(&circularBuffer);
    EXPECT_EQ(totalSize, DATA_SIZE_MEDIUM);

    CircBuffDrop(&circularBuffer, 1u);
    totalSize = CircBuffGetTotalSize(&circularBuffer);
    EXPECT_EQ(totalSize, DATA_SIZE_MEDIUM);

    CircBuffDrop(&circularBuffer, DATA_SIZE_MEDIUM);
    totalSize = CircBuffGetTotalSize(&circularBuffer);
    EXPECT_EQ(totalSize, DATA_SIZE_MEDIUM);
}

TEST(CircBuffTest, BufferIsBufferFull)
{
    /* Input variables.
    ****************************************************************/
    circBuff_t circularBuffer = { 0 };
    bool isBufferFull = 0;
    uint8_t dataBuffer[DATA_SIZE_MEDIUM] = { 0 };

    uint8_t writeBufferA[DATA_SIZE_MEDIUM] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14 };
    uint8_t writeBufferB[DATA_SIZE_VERY_SHORT] = { 0x70, 0x71, 0x72, 0x73, 0x74 };

    /* Test.
    ****************************************************************/
    CircBuffStaticBufferInit(&circularBuffer, dataBuffer, DATA_SIZE_MEDIUM);

    CircBuffWrite(&circularBuffer, writeBufferA, DATA_SIZE_MEDIUM);
    isBufferFull = CircBuffIsFull(&circularBuffer);
    EXPECT_EQ(isBufferFull, true);

    CircBuffDrop(&circularBuffer, 1u);
    isBufferFull = CircBuffIsFull(&circularBuffer);
    EXPECT_EQ(isBufferFull, false);

    CircBuffWrite(&circularBuffer, writeBufferB, DATA_SIZE_MEDIUM);
    isBufferFull = CircBuffIsFull(&circularBuffer);
    EXPECT_EQ(isBufferFull, true);

    CircBuffDrop(&circularBuffer, DATA_SIZE_VERY_SHORT);
    isBufferFull = CircBuffIsFull(&circularBuffer);
    EXPECT_EQ(isBufferFull, false);
}
