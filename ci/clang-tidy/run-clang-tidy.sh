#!/bin/bash

set -e

if test -z "$1"; then
    echo "Usage: $0 <dir> [<parameters...>]"
    echo " e.g.: $0 core -p build-ubuntu"
    exit
fi

ARGS=--extra-arg=-D__clang_analyzer__

FILES=`find $1 -type f`

#if test -z "$C_INCLUDE_PATH"; then
    # Default for our Docker image
#    echo "Setting C_INCLUDE_PATH=/opt/atollic/lib/gcc/arm-atollic-eabi/6.3.1/include/"
#    export C_INCLUDE_PATH=/opt/atollic/lib/gcc/arm-atollic-eabi/6.3.1/include/
#fi

shift

TOTAL_WARNINGS=0
TOTAL_ERRORS=0
TOTAL_WARNINGS_FILES=0
TOTAL_ERRORS_FILES=0

TOTAL_FILES=0
TOTAL_LINES=0

while read -r file; do
    #
    # Exclude directories
    #
    if [[ $file == *$1/nanopb* ]] || [[ $file == *$1/external/* ]]; then
       continue
    fi

    #
    # Exclude file types
    #
    if [[ $file != *\.c ]] && [[ $file != *\.h ]]; then
       continue
    fi

    echo "clang-tidy $ARGS $@ $file"
    TEXT=`clang-tidy $ARGS $@ $file 2>/dev/null`
    TOTAL_FILES=$(($TOTAL_FILES + 1))
    LINES=`cat $file | wc -l`
    TOTAL_LINES=$(($TOTAL_LINES + $LINES))

    if ! test -z "$TEXT"; then
        echo "$TEXT"
        WARNINGS=`echo "$TEXT" | grep " warning: " | wc -l`

        if test $(( $WARNINGS > 0 )); then
            TOTAL_WARNINGS=$(($TOTAL_WARNINGS + $WARNINGS))
            TOTAL_WARNINGS_FILES=$(($TOTAL_WARNINGS_FILES + 1))
        fi

        ERRORS=`echo "$TEXT" | grep " error: " | wc -l`

        if test $(( $ERRORS > 0 )); then
            TOTAL_ERRORS=$(($TOTAL_ERRORS + $ERRORS))
            TOTAL_ERRORS_FILES=$(($TOTAL_ERRORS_FILES + 1))
        fi
    fi
done <<< "$FILES"

echo ""
echo ""
echo "Summary:"
echo "  Files scanned:  $TOTAL_FILES with total lines (not code lines!): $TOTAL_LINES"
echo "  Total warnings: $TOTAL_WARNINGS in $TOTAL_WARNINGS_FILES files"
echo "  Total errors:   $TOTAL_ERRORS in $TOTAL_ERRORS_FILES files"
echo ""

if [ "$TOTAL_WARNINGS" -ne 0 ] || [ "$TOTAL_ERRORS" -ne 0 ]; then
    exit 1
fi

exit 0
