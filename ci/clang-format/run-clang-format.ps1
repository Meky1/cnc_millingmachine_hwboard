# Set variables
$FolderPathDev = "CoreDev"
$FolderPathTests = "Tests/TestSource"

# Loop 1 - Iterate through each subfolder of $FolderPathDev
Get-ChildItem -Path $FolderPathDev -File -Recurse |
    Where-Object { $_.Extension -eq ".c" -or $_.Extension -eq ".h" } |
        ForEach-Object {
            $path = Resolve-Path -relative $_.FullName
            Write-Output "clang-format $path"
            clang-format -i $path
}

# Loop 2 - Iterate through each subfolder of $FolderPathTests
Get-ChildItem -Path $FolderPathTests -File -Recurse |
    Where-Object { $_.Extension -eq ".cpp" -or $_.Extension -eq ".h" } |
        ForEach-Object {
            $path = Resolve-Path -relative $_.FullName
            Write-Output "clang-format $path"
            clang-format -i $path
}