#!/bin/bash

set -e

if test -z "$1"; then
    echo "Usage: $0 <SOURCE_BASE_DIR>"
    exit -1
fi

FILES=`find $1 -type f`

while read -r file; do
    if [[ $file == *.c ]] || [[ $file == *.h ]] || [[ $file == *.cpp ]]; then
        echo "clang-format $file"
        clang-format -i $file
    fi
done <<< "$FILES"

CHANGES=`git diff`

if ! test -z "$CHANGES"; then
    echo ""
    echo ""
    echo "Clang-format introduced following changes:"
    echo ""
    echo ""
    echo "$CHANGES"
    exit 1;
fi
