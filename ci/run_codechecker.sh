#!/usr/bin/env bash

# Log your project.
CodeChecker log -b "make" -o ./compilation_database.json

# Analyze your project.
CodeChecker analyze --ctu \
  -o ./reports \
  ./compilation_database.json

# Create the report file by using the CodeChecker parse command.
CodeChecker parse \
  --trim-path-prefix $(pwd) \
  -e codeclimate \
  ./reports > gl-code-quality-report.json

# Exit with status code 1 if there is any report in the output file.
status=$(cat gl-code-quality-report.json)
if [[ -n "$status" && "$status" != "[]" ]]; then
  exit 1
fi