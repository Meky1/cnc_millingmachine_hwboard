/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MD_MOTOR_X_ENC_A_Pin GPIO_PIN_0
#define MD_MOTOR_X_ENC_A_GPIO_Port GPIOA
#define MD_MOTOR_X_ENC_B_Pin GPIO_PIN_1
#define MD_MOTOR_X_ENC_B_GPIO_Port GPIOA
#define MD_MOTOR_Y_ENC_A_Pin GPIO_PIN_6
#define MD_MOTOR_Y_ENC_A_GPIO_Port GPIOA
#define MD_MOTOR_Y_ENC_B_Pin GPIO_PIN_7
#define MD_MOTOR_Y_ENC_B_GPIO_Port GPIOA
#define _MD_MOTOR_X_STBY_RST_Pin GPIO_PIN_5
#define _MD_MOTOR_X_STBY_RST_GPIO_Port GPIOC
#define MD_MOTOR_X_SW_Pin GPIO_PIN_0
#define MD_MOTOR_X_SW_GPIO_Port GPIOB
#define MD_MOTOR_X_SW_EXTI_IRQn EXTI0_IRQn
#define _MD_MOTOR_X_FLAG_Pin GPIO_PIN_1
#define _MD_MOTOR_X_FLAG_GPIO_Port GPIOB
#define _MD_MOTOR_X_BUSY_Pin GPIO_PIN_2
#define _MD_MOTOR_X_BUSY_GPIO_Port GPIOB
#define _MD_MOTOR_X_CS_Pin GPIO_PIN_11
#define _MD_MOTOR_X_CS_GPIO_Port GPIOF
#define _MD_MOTOR_Y_STBY_RST_Pin GPIO_PIN_14
#define _MD_MOTOR_Y_STBY_RST_GPIO_Port GPIOF
#define MD_MOTOR_Y_SW_Pin GPIO_PIN_15
#define MD_MOTOR_Y_SW_GPIO_Port GPIOF
#define MD_MOTOR_Y_SW_EXTI_IRQn EXTI15_10_IRQn
#define _MD_MOTOR_Y_FLAG_Pin GPIO_PIN_0
#define _MD_MOTOR_Y_FLAG_GPIO_Port GPIOG
#define _MD_MOTOR_Y_BUSY_Pin GPIO_PIN_1
#define _MD_MOTOR_Y_BUSY_GPIO_Port GPIOG
#define _MD_MOTOR_Y_CS_Pin GPIO_PIN_7
#define _MD_MOTOR_Y_CS_GPIO_Port GPIOE
#define _MD_MOTOR_Z_STBY_RST_Pin GPIO_PIN_15
#define _MD_MOTOR_Z_STBY_RST_GPIO_Port GPIOB
#define MD_MOTOR_Z_SW_Pin GPIO_PIN_8
#define MD_MOTOR_Z_SW_GPIO_Port GPIOD
#define MD_MOTOR_Z_SW_EXTI_IRQn EXTI9_5_IRQn
#define _MD_MOTOR_Z_FLAG_Pin GPIO_PIN_9
#define _MD_MOTOR_Z_FLAG_GPIO_Port GPIOD
#define _MD_MOTOR_Z_BUSY_Pin GPIO_PIN_10
#define _MD_MOTOR_Z_BUSY_GPIO_Port GPIOD
#define _MD_MOTOR_Z_CS_Pin GPIO_PIN_11
#define _MD_MOTOR_Z_CS_GPIO_Port GPIOD
#define MD_MOTOR_Z_ENC_A_Pin GPIO_PIN_12
#define MD_MOTOR_Z_ENC_A_GPIO_Port GPIOD
#define MD_MOTOR_Z_ENC_B_Pin GPIO_PIN_13
#define MD_MOTOR_Z_ENC_B_GPIO_Port GPIOD
#define DRILL_MOTOR_ENC_A_Pin GPIO_PIN_6
#define DRILL_MOTOR_ENC_A_GPIO_Port GPIOC
#define DRILL_MOTOR_ENC_B_Pin GPIO_PIN_7
#define DRILL_MOTOR_ENC_B_GPIO_Port GPIOC
#define DRILL_MOTOR_PWM_Pin GPIO_PIN_8
#define DRILL_MOTOR_PWM_GPIO_Port GPIOA
#define UART_LOG_TX_Pin GPIO_PIN_9
#define UART_LOG_TX_GPIO_Port GPIOA
#define UART_LOG_RX_Pin GPIO_PIN_10
#define UART_LOG_RX_GPIO_Port GPIOA
#define UART_COMM_TX_Pin GPIO_PIN_10
#define UART_COMM_TX_GPIO_Port GPIOC
#define UART_COMM_RX_Pin GPIO_PIN_11
#define UART_COMM_RX_GPIO_Port GPIOC
#define LED_RED_Pin GPIO_PIN_0
#define LED_RED_GPIO_Port GPIOD
#define LED_GREEN_Pin GPIO_PIN_1
#define LED_GREEN_GPIO_Port GPIOD
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
